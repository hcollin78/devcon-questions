# Cinia Questionary

This project was made for Cinia DEVcon 2018 to work as a questionary app in workshop. It also worked as a personal practise project for multiple new technologies like Mobx-state-tree, xstate, Websockets, express, node, docker and Google Cloud. This list will most likely grow before D-day. /end of rant

## Development

### Installation

Clone the repository first to your preferred working directory.

Install dependencies with either npm or yarn for both front end and backend separately.

/cq-server/yarn install 
/cq-front/yarn install 

### Running Server

Start the server first with npm or node in directory /cq-server/:

npm start 

or

node server.js

This will start an express server on port 80 that will will server the latest production build of the front end. This is not used development.

It will also start another server that will listen to WebSocket calls on port 8080. This will function as the main backend for the application.

### Running front end

Start the development server for front end in /cq-front/ with:

yarn start


## Production

To make a production version do following steps:

1) Clean possible unnecessary node_modules

/cq-front/yarn autoclean --force

2) Compile production bundle of the front end

/cq-front/yarn build

3) Make a docker image out of the whole system

/cq-server/docker build -t cinia-quiz .

4) Test the image by running it and seeing that the service is running in port 80

/cq-server/docker run -it -p 80:80 -p 8080:8080 cinia-quiz


### Deploying to Google Cloud


docker tag cinia-quiz eu.gcr.io/devcon-2018-questionary-server/quiz

gcloud auth configure-docker

docker push eu.gcr.io/devcon-2018-questionary-server/quiz

gcloud container images list-tags eu.gcr.io/devcon-2018-questionary-server/quiz

