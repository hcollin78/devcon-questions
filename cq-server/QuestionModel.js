const { types, actions, views, destroy } = require('mobx-state-tree');
const uuid = require('uuid');
const AnswerModel = require('./AnswerModel');

const QuestionModel = types.model({
    id: types.identifier(types.string),
    question: types.string,
    description: types.maybe(types.string),
    answers: types.optional(types.array(AnswerModel), [])
}).actions(self => ({
    update: (params) => {
        self.question = params.question;
        self.description = params.description;
        return self;
    },
    createAnswer: (params) => {
        const id = uuid.v4();
        const answer = AnswerModel.create({
            id: id,
            text: params.text,
            value: params.value,
            nextQuestionId: params.nextQuestionId
        });
        self.answers.push(answer);
        return answer;
    },
    removeAnswer: (params) => {
        const answer = self.answers.find(ans => ans.id === params.id);
        destroy(answer);
        return {answer: params.id, removed: true};
    }
})).views(self => ({
    getAnswer: (answerId) => {
        return self.answers.find(ans => ans.id === answerId);
    }

}));

module.exports = QuestionModel;




