const { types, actions, views } = require('mobx-state-tree');

// const { QuestionaryModel } = require('./QuestionaryModel');
// const { QuestionModel } = require('./QuestionModel');
// const { AnswerModel } = require('./AnswerModel');
// const { UserModel } = require('./UserModel');



const UserAnswerModel = types.model({
    questionary: types.string,
    question: types.string,
    user: types.string,
    answer: types.string,
    answered: types.Date
}).actions(self => ({
    updateAnswer: (params) => {
        self.answer = params.answer.id;
        self.answered = new Date();
    }
}));

module.exports = UserAnswerModel;