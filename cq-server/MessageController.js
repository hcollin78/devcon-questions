const { getSnapshot } = require('mobx-state-tree');
const { sha256 } = require('js-sha256');

class MessageController {

    constructor() {
        this.store = null;
        this.server = null;
    }

    setStore(store) {
        this.store = store;
    }

    setServer(server) {
        this.server = server;
    }

    handleClientMessage(climsg, connectionId) {
        // console.log("Handle client message ", climsg, " from ", connectionId);
        const { key, data, reply } = JSON.parse(climsg);
        // console.log(key, data, reply);
        switch(key) {
            case "ping":
                return this.server.replyMessageWrapper(connectionId, "pong", reply);
            case "getCurrentState":
                return this.server.replyMessageWrapper(connectionId, this.store.getInitialState(), reply);
            case "getFullSnapShot":
                return this.server.replyMessageWrapper(connectionId, this.store.getFullSnapShot(), reply);
            case "setFullSnapShot":
                return this.server.replyMessageWrapper(connectionId, this.store.setFullSnapShot(data), reply);


            case "createLocalUser":
                return this.server.replyMessageWrapper(connectionId, this.store.createLocalUser({connId: connectionId}), reply);
            case "createKioskUser":
                return this.server.replyMessageWrapper(connectionId, this.store.createKioskUser(), reply);
            case "sudoAsAdmin": 
                return this.server.replyMessageWrapper(connectionId, this.checkSudoPassword(connectionId, data.pwd), reply);
            case "getUserById": 
                return this.server.replyMessageWrapper(connectionId, this.store.getUser(connectionId, data.userId), reply);
                

            case "reloadQuestionaries":
                return this.server.replyMessageWrapper(connectionId, this.store.getInitialState(), reply);

            case "addAnswer":
                return this.server.replyMessageWrapperWithBroadcast(connectionId, this.store.addAnswer(data), reply, "broadcastNewAnswer");
            case "getAnswersToQuestionary":
                return this.server.replyMessageWrapper(connectionId, this.store.getAnswersToQuestionary(data), reply);
            case "completeQuestionary":
                return this.server.replyMessageWrapper(connectionId, this.store.completeQuestionary(data), reply);

            case "createQuestionary":
                return this.server.replyMessageWrapper(connectionId, this.store.createQuestionary(data), reply);
            case "updateQuestionary":
                return this.server.replyMessageWrapperWithBroadcast(connectionId, this.store.updateQuestionary(data), reply, "broadcastQuestionaryUpdate");
            case "removeQuestionary":
                return this.server.replyMessageWrapper(connectionId, this.store.removeQuestionary(data), reply);

            case "createQuestion":
                return this.server.replyMessageWrapper(connectionId, this.store.createQuestion(data), reply);
            case "updateQuestion":
                return this.server.replyMessageWrapperWithBroadcast(connectionId, this.store.updateQuestion(data), reply, "broadcastQuestionaryUpdate");
            case "removeQuestion":
                return this.server.replyMessageWrapper(connectionId, this.store.removeQuestion(data), reply);

            case "createAnswer":
                return this.server.replyMessageWrapper(connectionId, this.store.createAnswer(data), reply);
            case "updateAnswer":
                return this.server.replyMessageWrapperWithBroadcast(connectionId, this.store.updateAnswer(data), reply, "broadcastQuestionaryUpdate");
            case "removeAnswer":
                return this.server.replyMessageWrapper(connectionId, this.store.removeAnswer(data), reply);

            case "testEcho":
                return this.server.replyMessageWrapper(connectionId, {data}, reply);

            case "testError":
                return this.server.replyMessageWrapper(connectionId, { error: "TEST ERROR", data: data}, reply);

            default:
                break;
        }
        return false;
    }

    // Check the admin password validity
    checkSudoPassword(connId, clientPass) {
        const passwd = sha256(connId + "-" + "1254");
        return (clientPass === passwd ? "OK": "FAIL" );
    }
}

module.exports = MessageController;