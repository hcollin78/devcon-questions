const { types } = require('mobx-state-tree');

const AnswerModel = types.model({
    id: types.identifier(types.string),
    text: types.string,
    value: types.string,
    nextQuestionId: types.maybe(types.string)
}).actions(self => ({
    update: (params) => {
        self.text = params.text;
        self.value = params.value;
        self.nextQuestionId = params.nextQuestionId;
        return self;
    }
}));

module.exports = AnswerModel;




