const { types, actions, views } = require('mobx-state-tree');


const UserModel = types.model({
    id: types.identifier(types.string),
    email: types.maybe(types.string),
    login: types.maybe(types.string),
    password: types.maybe(types.string),
    name: types.maybe(types.string),
    role: types.optional(types.enumeration(["ANONYMOUS", "ADMIN", "LOCALUSER", "KIOSK", "USER"]), "ANONYMOUS"),
    completedQuestionaries: types.optional(types.array(types.string), [])
}).actions(self => ({
    isValidlogin: (login, password) => {

    },
    addCompletedQuestionary: (questionaryId) => {
        self.completedQuestionaries.push(questionaryId);
    }
}));

module.exports = UserModel;