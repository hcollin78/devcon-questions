const { types, actions, views, getSnapshot, applySnapshot, destroy, detach } = require('mobx-state-tree');
const uuid = require('uuid');


const QuestionaryModel = require('./QuestionaryModel');
const UserModel = require('./UserModel');
const UserAnswerModel = require('./UserAnswerModel');

const StoreModel = types.model({
    questionaries: types.optional(types.map(QuestionaryModel), {}),
    users: types.optional(types.map(UserModel), {}),
    answers: types.optional(types.array(UserAnswerModel), [])
}).actions(self => ({

    // QUESTIONARIES
    createQuestionary: (params) => {
        console.log("Create Questionary", params);
        const id = uuid.v4();
        const newQs = QuestionaryModel.create({
            id: id,
            name: params.name,
            visibility: params.visibility,
            type: params.type,
            description: params.description,
            questions: {},
            startQuestion: null,
            state: "DRAFT"
        });
        self.questionaries.put(newQs)
        return newQs;
    },
    removeQuestionary: (params) => {
        console.log("Remove Questionary", params);
        const questionary = self.questionaries.get(params.id);
        detach(questionary);
        return true;

    },
    updateQuestionary: (params) => {
        const questionary = self.questionaries.get(params.id);
        if(questionary) {
            return questionary.update(params);
        }
    },
    setNextQuestion: (params) => {
        const questionary = self.questionaries.get(params.questionaryid);
        if(questionary) {
            return questionary.setNextQuestion(params);
        }
    },

    // QUESTIONS
    updateQuestion: (params) => {
        const questionary = self.questionaries.get(params.questionaryId);
        if(questionary) {
            const question = questionary.questions.get(params.id);
            if(question) {
                return question.update(params);
            }
            console.log("\n\nERROR: Cannot update a question  " + params.id +". Question not found.\n\n");
            return {error: "Cannot update question " + params.id + ". Question not found.", data: params};    
            
        }
        console.log("\n\nERROR: Cannot update a question for questionary " + params.questionaryId +". Questionary not found.\n\n");
        return {error: "Cannot update question for questionary. Questionary not found.", data:params};
    },
    createQuestion: (params) => {
        const questionary = self.questionaries.get(params.questionaryId);
        if(questionary) {
            return questionary.createQuestion(params);
        }
        console.log("\n\nERROR: Cannot create a question for questionary " + params.questionaryId +". Questionary not found.\n\n");
        return {error: "Cannot create question for questionary. Questionary not found.", data:params};
    },
    removeQuestion: (params) => {
        const questionary = self.questionaries.get(params.questionaryId);
        if(questionary) {
            //TODO: This should also go through all the answers on remaining questions with loose connection to this 
            // removed question and turn that connection (nextQuestionId) to 'THE_END'
            return questionary.removeQuestion(params);
        }
        console.log("\n\nERROR: Cannot remove a question for questionary " + params.id +".\n\n");
        return {error: "Cannot remove question for questionary.", data:params};
    },


    // ANSWERS (AS OPTIONS IN QUESTIONS)
    createAnswer: (params) => {
        const questionary = self.questionaries.get(params.questionaryId);
        if(questionary) {
            const question = questionary.questions.get(params.questionId);
            if(question) {
                return question.createAnswer(params);
            }
        }
        console.log("\n\nERROR: Cannot create an answer for a question " + params.questionId +".\n\n");
        return {error: "Cannot create an answer for question.", data:params};
    },
    updateAnswer: (params) => {
        const questionary = self.questionaries.get(params.questionaryId);
        if(questionary) {
            const question = questionary.questions.get(params.questionId);
            if(question) {
                const answer = question.getAnswer(params.id);
                if(answer) {
                    return answer.update(params);
                }
                
            }
        }
        console.log("\n\nERROR: Cannot update an answer " + params.id +".\n\n");
        return {error: "Cannot update an answer.", data:params};
    },
    removeAnswer: (params) => {
        const questionary = self.questionaries.get(params.questionaryId);
        if(questionary) {
            const question = questionary.questions.get(params.questionId);
            if(question) {
                return question.removeAnswer(params);
            }
        }
        console.log("\n\nERROR: Cannot remove an answer " + params.id +".\n\n");
        return {error: "Cannot remove an answer.", data:params};
    },

    // USER ANSWERS TO QUESTION
    addAnswer: (params) => {
        console.log("PARAMS", params);
        const data = params.answer;
        const previousAnswer = self.answeredAlready(data.questionary.id, data.question.id, data.user.id);
        if(previousAnswer) {
            previousAnswer.updateAnswer(data);
            return previousAnswer;
        } else {
            const answer = UserAnswerModel.create({
                questionary: data.questionary.id,
                question: data.question.id,
                answer: data.answer.id,
                user: data.user.id,
                answered: new Date()
            });
            self.answers.push(answer);
            return answer;
        }
    },
    completeQuestionary: (params) => {
        const user = self.users.get(params.userId);
        user.addCompletedQuestionary(params.questionaryId);
        return params;
    },

    answeredAlready(questionaryId, questionId, userId) {
        return self.answers.find(ans => ans.questionary === questionaryId && ans.question === questionId && ans.user === userId);
    },

    // Create normal user
    createUser: (param) => {
        console.log("Add user", param);
    },

    // Local user is a user that is tied to the browsers local storage, but not really logged in.
    createLocalUser: (params) => {
        const user = UserModel.create({
            id: "ul-"+params.connId,
            email: "",
            login: "",
            password: "",
            name: "",
            role: "LOCALUSER"
        });
        self.users.put(user);
        return user;
    },
    // Creates a user that is never retrieved as valid relogin
    createKioskUser: (params) => {
        const user = UserModel.create({
            id: "ki-"+uuid.v4(),
            email: "",
            login: "",
            password: "",
            name: "",
            role: "KIOSK"
        });
        self.users.put(user);
        return user;
    },
    getUser: (connId, userId) => {
        const user = self.users.get(userId);
        console.log("user", user);
        if(user) {
            return user;
        }
        return self.createUser({connId: connId});

    },

    /**
     * This is used to restore the state of the system after docker image reset.
     * THIS WILL OVERRIDE EVERYTHING ON THE SERVER WITHOUT FALLBACK.
     * 
     * USE WITH CARE!
     */
    setFullSnapShot: (data) => {
        console.log("Set full snapshot from data", data);
        try {
            applySnapshot(self, data);
            return {ok: true};
        } catch(err) {
            return {error: err};
        }
        
    },

    updateUser: (params) => {
        console.log("Update User", params);
    },
    login: (username, password) => {
        console.log("Login");
    },
    logout: (params) => {
        console.log("Logout", params);
    },
    anonymousLogin: (params) => {
        console.log("Anonymous login", params);
    }
    
})).views(self => ({
    getInitialState: () => {
        console.log("Get initial State");  
        const sh = getSnapshot(self);
        return {questionaries: sh.questionaries};
    },

    getFullSnapShot: () => {
        return getSnapshot(self);
    },

    getAnswersToQuestionary: (params) => {
            console.log("Get answers for questionary", params.questionaryId, self.answers.toJSON());
            const answerList = self.answers.filter(ans => ans.questionary === params.questionaryId);
            return {answers: answerList};
    }
}));

module.exports = StoreModel;