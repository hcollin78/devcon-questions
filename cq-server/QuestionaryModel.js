const { types, actions, destroy } = require('mobx-state-tree');
const uuid = require('uuid');

const QuestionModel = require('./QuestionModel');
const UserModel = require('./UserModel');

const QuestionaryModel = types.model({
    id: types.identifier(),
    name: types.string,
    visibility: types.optional(types.enumeration(["PUBLIC", "INVITATION", "USER"]), "PUBLIC"),
    type: types.optional(types.enumeration(["GAMESHOW", "OPEN", "TIMED"]), "OPEN"),
    description: types.optional(types.string, ""),
    questions: types.optional(types.map(QuestionModel), {}),
    startQuestion: types.maybe(types.reference(QuestionModel)),
    state: types.enumeration(["DRAFT", "OPEN", "CLOSED", "ARCHIVED"]),
    
    // These types are for GAMESHOW and TIMED modes
    currentQuestion: types.maybe(types.reference(QuestionModel)),
    timePerQuestion: types.maybe(types.number),
    startTime: types.maybe(types.Date),
    endTime: types.maybe(types.Date),
    hostUserId: types.maybe(types.string)
    
}).actions(self => ({

    update: (params) => {
        self.name = params.name;
        self.visibility = params.visibility;
        self.type = params.type;
        self.description = params.description;
        self.state = params.state;
        self.timePerQuestion = params.timePerQuestion ? params.timePerQuestion : null;
        self.startTime = params.startTime ? params.startTime : null;
        self.endTime = params.endTime ? params.endTime : null;
        self.hostUserId = params.hostUserId ? params.hostUserId : null;


        if(params.startQuestion) {
            const targetQuestion = self.questions.get(params.startQuestion);
            if(targetQuestion) {
                self.startQuestion = targetQuestion;
            } else {
                console.log("\n\nERROR: Target question failed!", targetQuestion);
            }
        }
        
        if(params.currentQuestion) {
            const currentQuestion = self.questions.get(params.currentQuestion);
            if(currentQuestion) {
                self.currentQuestion = currentQuestion;
            } else {
                self.currentQuestion = self.currentQuestion ? self.currentQuestion : null;
            }
        }

        
        return true;
    },

    createQuestion: (params) => {
        const id = uuid.v4();
        const question = QuestionModel.create({
            id: id,
            question: params.question,
            description: params.description,
            answers: []
        });
        self.questions.put(question);
        return question;
    },
    removeQuestion: (params) => {
        const question = self.questions.get(params.id)
        if(question) {
            destroy(question);
            return true;
        }
        return { error: "Cannot remove a question " + params.id + ". Question not found.", data: params, self: self, question: question};
        
    },
    setNextQuestion: (params) => {
        if(params.questionId) {
            const currentQuestion = self.questions.get(params.questionId);
            self.currentQuestion = currentQuestion;
            return { questionary: self };
        }
    },
    
    // This chooses the next question to be the one assingned to the answer with most votes.
    automaticNextQuestionSelector: (params) => {
        //TODO: Will be done if and when necessary and there is time.
    }

})).views(self => ({
    getQuestionById: (questionId) => {
        console.log("get Question By Id", questionId);
    },
    listQuestions: () => {
        console.log("List Questions");
    }
}));

module.exports = QuestionaryModel;




