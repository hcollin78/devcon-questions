const uuid = require('uuid');
const WebSocket = require('ws');
const fs = require('fs');
const express = require('express');




const  { onSnapshot } = require('mobx-state-tree');

const StoreModel = require('./StoreModel');
const initialData = require('./data/initialData.json');
const MessageController = require("./MessageController");

const defaultConfig = {
    port: 8080
};

class Server {

    constructor(config) {
        this.config = config;
        this.connections = [];
        this.messageController = null;
        this.server = null;
    }

    setMessageController(handler) {
        this.messageController = handler;
        handler.setServer(this);
    }

    setStore(store) {
        this.store = store;
    }

    startServer() {
        this.server = new WebSocket.Server({ port: this.config.port });
        console.log("\nServer started at port ", this.config.port);
        this.server.on('connection', (ws) => this.onConnection(ws));
    }

    onMessage(fromConnectionId, message) {
        if(message.length > 2000) {
            console.log("\nINCOMING:\n\tFrom:\t", fromConnectionId, "\n\tMessage: Message length too long!\n");
        } else {
            console.log("\nINCOMING:\n\tFrom:\t", fromConnectionId, "\n\tMessage:", message, "\n");
        }
        
        const replyObj = this.messageController.handleClientMessage(message, fromConnectionId);
        if(replyObj) {
            this.send(replyObj.to, replyObj.key, replyObj.data);
        }
    }

    onConnection(ws) {
        
        const connId = uuid.v4();

        const connectionObject = {
            connection: ws,
            id: connId
        };
        console.log("\nCONNECTION\n\tId:\t", connId, "\n");

        connectionObject.connection.on('message', (msg) => this.onMessage(connId, msg));

        this.connections.push(connectionObject);

        this.send(connectionObject.id, "connectionEstablished", {connectionId: connectionObject.id});
    }

    replyMessageWrapper(connectionId, data, replyKey) {
        return {
            key: replyKey,
            data: data,
            to: connectionId
        };
    }

    replyMessageWrapperWithBroadcast(connectionId, data, replyKey, broadCastKey) {
        
        this.broadcast(broadCastKey, {key: broadCastKey, data: data, broadcast: true})
        
        return {
            key: replyKey,
            data: data,
            to: connectionId
        };
    }

    send(connectionId, key, data) {
        const cobj = this.connections.find(con => con.id === connectionId);
        // if(cobj === undefined) {
        //     console.log("\nERROR: Connection does not exist anymore.\n\tkey:\t" + connectionId);
        //     return;
        // }
        if(data && data.length > 2000) {
            console.log("\nSEND:\n\tKey:\t", key, "\n\tTo:\t", connectionId, "\n\tData:\tData set too large!\n");    
        } else {
            console.log("\nSEND:\n\tKey:\t", key, "\n\tTo:\t", connectionId, "\n\tData:\t", data, "\n");    
        }
        // console.log("\nSEND:\n\tKey:\t", key, "\n\tTo:\t", connectionId, "\n\tData:\t", data, "\n");
        const msgObj = {
            key: key,
            data: data
        };
        cobj.connection.send(JSON.stringify(msgObj));
    }

    broadcast(key, data) {
        const msg = JSON.stringify({
            key: key,
            data: data,
            broadcast: true
        });
        console.log("\nBROADCAST\n\tKey:\t", key);
        this.connections.forEach(cobj => {
            if(cobj.connection.readyState === WebSocket.OPEN) {
                console.log("\t\tto:", cobj.id);
                cobj.connection.send(msg);    
                
            } else {
                //TODO: REmove dead connection
            }
            
            this.removeClosedConnections();
        });
    }

    removeClosedConnections() {
        console.log("\nCLEAR:\n\tCurrent count:", this.connections.length);
        this.connections = this.connections.filter(cobj => {
            console.log("\t\tid:", cobj.id, "\n\tconnection on ", cobj.connection.readyState === WebSocket.OPEN);
            return cobj.connection.readyState === WebSocket.OPEN;
        });
        console.log("\tAfter clearing:", this.connections.length);
    }
}

/**
 * Load the current state from disk when starting server
 * @param {*} filename 
 * @param {*} defaultState 
 */
function loadStoreFromDisk(filename, defaultState) {
    try {
        const currentState = fs.readFileSync(filename);
        console.log("\nLOADING STATE:\n\tfile:\t", filename);
        return JSON.parse(currentState.toString());
    } catch(err) {
        const newFileName = filename + uuid.v4() + ".old";
        fs.copyFileSync(filename, newFileName);
        console.log("\n\n\n==========================\n\nERROR: Could not read or parse the old file as valid state!\n\tBackup filename:", newFileName, "\n\n================================\n\n\n");
        return defaultState;    
    }
}

/**
 * Save current state to disk
 * @param {*} store 
 * @param {*} filename 
 */
function saveStoreToDisk(snapShot, filename) {
    fs.writeFile(filename, JSON.stringify(snapShot), 'utf8', (err) => {
        if(err) {
            console.log("\n\n\nERROR IN WRITING FILE!", err);
        } else {
            console.log("\nSnapshot saved!");
        }

    });
    
}


const dataFile = "./data/current.json";

const store = StoreModel.create(loadStoreFromDisk(dataFile, initialData));
console.log("\nSTORE STATE\n\tQuestionaries:\t", store.questionaries.size, "\n\tAnswers:\t", store.answers.size, "\n\tUsers:\t\t", store.users.size, "\n\n");


const controller = new MessageController();
controller.setStore(store);

const server = new Server(defaultConfig);
server.setMessageController(controller);

onSnapshot(store, (snap) => {    
    saveStoreToDisk(snap, dataFile);
});

server.startServer();

// Simple http server too...
const expressApp = express();
expressApp.use('/', express.static('public'));
// expressApp.get("/", (req, res) => res.send('Hello World'));

expressApp.listen(80, () => console.log("Listening to you in port 80."));

