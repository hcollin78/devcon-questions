import React from 'react';
// import { observable } from 'mobx';
import { observer, inject } from 'mobx-react';

// import './App.css';

import AdminView from './views/AdminView';
import IndexView from './views/IndexView';
import QuestionariesView from './views/QuestionariesView';
import QuestionView from './views/QuestionView';
import QuestionaryView from './views/QuestionaryView';

import StatisticsView from './views/StatisticsView';
import ResultsView from './views/ResultsView';
import LoginView from './views/LoginView';
import LoadingScreenView from './views/LoadingScreenView';

// import Header from './components/headers/Header';
import MainMenu from './components/mainmenu/MainMenu';
// import AdminMenu from './components/adminmenu/AdminMenu';

import ConnectionIndicator from './components/connectionIndicator/ConnectionIndicator';
import InfoBox from './components/infobox/InfoBox';

import NotificationContainer from './components/notifier/NotifierView';
import './styles/default-page.scss';


@inject("store", "viewMachine", "ux")
@observer
export default class App extends React.Component {


  // handleLogin = (e) => {

  //   const admin = this.props.store.getUserById("admin");
  //   console.log("LOGIN!", admin);
  //   if (admin) {
  //     this.props.store.ux.setUser(admin);
  //   }
  // };

  // handleLogout = () => {
  //   this.props.store.ux.setUser(null);
  // };


  render() {


  //  console.log("STATE:", this.props.store.ux.viewState);
   
    const views = {
      "INIT": <LoadingScreenView />,
      "INDEX": <IndexView />,
      "QUESTIONARIES": <QuestionariesView />,
      "QUESTIONARY": <QuestionaryView />,
      "QUESTION": <QuestionView />,
      "ADMIN": <AdminView />,
      "LOGIN": <LoginView />,
      "LOGIN_PENDING": <LoginView />,
      "MYRESULTS": <ResultsView />,
      "STATISTICS": <StatisticsView />
    };


    const view = views[this.props.ux.state === "INDEX" ? this.props.store.ux.defaultView() : this.props.ux.state];

    const containerViewModeClasses = ["main-container", this.props.store.ux.viewModeClass()].join(" ");

    if(this.props.ux.isLoading()) {
      return (
        <div className="App">
          <div className="default-page">
            <h1>Loading data...</h1>
          </div>
        </div>
      )
    }


    return (
      <div className="App">

        <ConnectionIndicator />

        <MainMenu />

        <div className={containerViewModeClasses}>
          {view}
        </div>

        <InfoBox />

        <NotificationContainer />
      </div>
    );
  }
}

