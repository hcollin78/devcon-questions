import React from 'react';
import ReactDOM from 'react-dom';
// import { Route, BrowserRouter } from 'react-router-dom';
import { Provider } from 'mobx-react';
import { getSnapshot, onSnapshot, applySnapshot, onPatch } from 'mobx-state-tree';
import makeInspectable from 'mobx-devtools-mst';

import App from './App';
//import registerServiceWorker from './registerServiceWorker';

import connection, { liveConfiguration } from './api/Connection';
import { getInitialState, getLocalState, listenToChangesInQuestionaries,reloadQuestionaries } from './api/StoreApi';
import { identifyUser, confirmSudoAdminPassword, storeUserToLocalStorage } from './api/UserApi';

import notificationService from './components/notifier/notificationService';

import { Store, defaultBaseState } from './models/store';
import  UxStore from './models/UxStore';
import ViewMachine from './machines/ViewMachine';

import './style.scss';

// Create new store with just the absolutely necessary data
const uxstore = UxStore.create();
uxstore.bootingUp();
const store = Store.create(defaultBaseState);


// Ugly rewire to separate UX STORE for both view and mode
// TODO: This needs to be removed when the actual ux mode and view handling is removed from the main store.
onPatch(store, (patch) => {
    // console.log("Patch", patch);
    switch(patch.path) {
        case "/ux/viewState":
            uxstore.setState(patch.value);     
            break;
        case "/ux/viewMode":
            uxstore.setMode(patch.value);
            break;
    }
});

// Setup main State Machine
const viewMachine = new ViewMachine(store);

// Open connection to server
connection.openConnection();

// Get initial state for the store from the server;
getInitialState().then(res => {
    const localSnapShot = getLocalState();
    let newState = {};
    if(localSnapShot) {
        newState = Object.assign({}, defaultBaseState, localSnapShot, res);
    } else {
        newState = Object.assign({}, defaultBaseState, res);
    }
    newState.ux.adminMode = false;
    
    
    newState.ux.viewState =  newState.ux.viewState === "ADMIN" ? "QUESTIONARIES" : newState.ux.viewState;
    applySnapshot(store, newState);    
    viewMachine.correctStateIfNecessary();

    // Load user
    identifyUser(store.ux.viewMode).then(user => {        
        if(user) {
            console.log("USER IDENTIFIED!", uxstore.isLoading(), uxstore.state, uxstore.mode);
            store.ux.setUser(user);
            uxstore.bootingDone(store.ux.viewState, store.ux.viewMode);
            storeUserToLocalStorage(user);
        }
    });

    listenToChangesInQuestionaries((reply) => {
        reloadQuestionaries().then(reply => {
            store.updateAllQuestionaries(reply.data);
            notificationService.info("Questions updated");

        });
    });
});



// Make the store inspectable by the mobx-dev-tools
// TODO: Remove before rollout
makeInspectable(store);

ReactDOM.render((
    <Provider store={store} viewMachine={viewMachine} ux={uxstore} >
            <App />
    </Provider>
), document.getElementById('root'));
//registerServiceWorker();
