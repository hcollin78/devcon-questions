import React from 'react';
import { observer, inject } from 'mobx-react';
import { observable } from 'mobx';

import qmachine from '../machines/QuestionaryMachine';

import SimpleList from '../components/list/SimpleList';

@inject("store", "viewMachine", "ux")
@observer
export default class QuestionaryView extends React.Component {

    @observable id = null;
    @observable questionary = null;


    constructor(props) {
        super(props);
        this.questionary = props.store.ux.questionary;
    }

    goToFirst = () => {
        this.doMachineAction("start");
    };

    back = () => {
        this.props.viewMachine.action("goToQuestionaries");
    }

    // This uses the custom QuestionMachine
    doMachineAction = (actionString) => {
        const action = qmachine.action(actionString, this.questionary, this.props.ux);
        if(action) this.props.store.qMachineActionHandler(action);

    };

    render() {

        if (this.questionary === null) {
            return (
                <div className="loading">
                    Loading...
                </div>
            )
        }

        if (this.questionary.state === "DRAFT") {
            return (
                <div className="default-page">
                    <h1>This questionary is does not exist!</h1>

                    <button onClick={() => { this.props.viewMachine.action("goToQuestionaries"); }}>Go back!</button>
                </div >
            );
        }


        console.log("Completed this questionary:", this.props.store.ux.user.hasCompletedQuestionary(this.questionary));

        const myAnswers = this.props.store.getMyAnswers(this.questionary);
        

        const answerOptions = {
            columns: [
                {
                    key: ["question", "question"],
                    headerText: "Question",
                    styleClass: ["half"]
                },
                {
                    key: ["answer", "text"],
                    headerText: "Answer",
                    styleClass: ["half"]
                }

            ]
        };

        return (
            <div className="default-page questionary-view">
                <h2 className="pre-title">Questionary</h2>
                <h1 className="page-title with-pre-title">{this.questionary.name}</h1>

                <p>{this.questionary.description}</p>

                <div className="action-bar">
                    <button onClick={this.back} >Back to Questionaries</button>
                    {/* <button onClick={this.resetAnswers} >Reset my Answers</button>
                    <button onClick={this.continueQuestionary} >Continue Questionary</button> */}
                    <button onClick={this.goToFirst} className="ok">Start Questionary</button>
                </div>

                {myAnswers && myAnswers.length > 0 && (
                    <div className="part answers">
                        <h2>My current answers</h2>
                        <SimpleList list={myAnswers} options={answerOptions} />
                    </div>
                )}



            </div>
        )
    }
}