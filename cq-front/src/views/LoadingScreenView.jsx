import React from 'react';
import { observer, inject } from 'mobx-react';

@inject("store", "viewMachine")
@observer
export default class LoadingScreenView extends React.Component {
    render() {

        return (
            <div className="default-page loading-screen">
                <h1>CONNECTING TO SERVER...</h1>
            </div>
        );
    }
}