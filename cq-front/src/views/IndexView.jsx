import React from 'react';
import { observer, inject } from 'mobx-react';

@inject("store", "viewMachine")
@observer
export default class IndexView extends React.Component {

    render() {

        return (
            <div className="index-view">
                Index View
            </div>
        );
    }
}