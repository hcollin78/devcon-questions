import React from 'react';
import { observer, inject } from 'mobx-react';
import { observable, action } from 'mobx';

import LabelInput from '../components/inputs/LabelInput';
import LabelSelect from '../components/inputs/LabelSelect';
import LabelButtons from '../components/inputs/LabelButtons';
import AdminButton from './admins/AdminButton';
import Saving from '../components/loaders/Saving';

import QuestionaryForm from './admins/QuestionaryForm';
import QuestionForm from './admins/QuestionForm';
import AnswerForm from './admins/AnswerForm';
import AdminSnapshot from './admins/AdminSnapshot';

import SimpleList from '../components/list/SimpleList';

import adminMachine from '../machines/AdminMachine';
import AdminEditStore from './admins/AdminStore';

import connection from '../api/Connection';
import { clearLocalState, getFullSnapShot } from '../api/StoreApi';

import notificationService from '../components/notifier/notificationService';

import ConfirmModal from '../components/modals/ConfirmModal';


import '../styles/admin-styles.scss';

@inject("store", "viewMachine")
@observer
export default class AdminView extends React.Component {

    @observable adminView = null;
    @observable defaultActions = {};

    @observable showConfirm = false;

    constructor(props) {
        super(props);

        this.adminView = adminMachine.currentState;

        adminMachine.setDefaultHandlers({
            "changeView": (params, newState) => {
                return this.changeView(newState.value);
            },
            "loadQuestionaryForEditing": (params, newState) => {
                return true;
            },
            "changeMode": (params, newState) => {
                return this.props.store.ux.setMode(params.newMode);
            },
            "saveStateToLocalStorage": (params, newState) => {
                return this.props.store.doAction("saveStateToLocalStorage");
            },
            "clearEditData": (params, newState) => {
                AdminEditStore.clearAll();
                return true;
            },
            "clearQuestionaryForm": (params, newState) => {
                return true;
            }
        });
    }

    @action
    transition = (actionString, params, actionHandlers = {}) => {
        const newState = adminMachine.transition(actionString, actionHandlers, params);
        this.changeView(newState.value);
        return newState;
    }

    @action
    asyncTransition = (actionString, params, actionHandlers = {}) => {
        const newState = adminMachine.asyncTransition(actionString, actionHandlers, params);
        this.changeView(newState.value);
        return newState;
    }

    @action
    changeView(toView) {
        this.adminView = toView;
        return true;
    }

    @action
    editQuestionary = (item) => {
        AdminEditStore.setQuestionary(item);
        this.transition("editQuestionary", {});
    }

    @action
    changeMode = (name, mode) => {
        this.transition("changeMode", { newMode: mode });
        notificationService.info("View mode changed to " + mode);

    }

    serverTest = () => {
        console.log("Ping?");
        connection.sendMessage("ping", { reply: "pong" }, (ret) => {
            console.log(ret.data);
        });
    }

    @action
    clearLocalStorage = () => {
        clearLocalState();
        notificationService.success("Localstorage cleared. Please refresh the browser.");
        this.showConfirm = false;

    }

    areYouSure = () => {
        this.showConfirm = true;
    }

    cancelClear = () => {
        this.showConfirm = false;
    }


    render() {

        if (this.adminView === "QUESTIONARY") {
            return (<QuestionaryForm onTransition={this.transition} onAsyncTransition={this.asyncTransition} questionary={AdminEditStore.questionary} />);
        }

        if (this.adminView === "QUESTION") {
            return (<QuestionForm onTransition={this.transition} onAsyncTransition={this.asyncTransition} question={AdminEditStore.question} />);
        }

        if (this.adminView === "ANSWER") {
            return (<AnswerForm onTransition={this.transition} onAsyncTransition={this.asyncTransition} answer={AdminEditStore.answer} />);
        }

        if (this.adminView === "SAVING") {
            return (<div className="default-page admin-view"><Saving /></div>)
        }

        const questionaryList = this.props.store.listQuestionariesAsArray();

        //const questionarySelectableList = questionaryList.map(qs => {return {value: qs.id, text: qs.name}});

        const listOptions = {
            columns: [
                {
                    key: "name",
                    headerText: "Name",
                    styleClass: ["w200px"]
                },
                {
                    key: "description",
                    headerText: "Description",
                    styleClass: ["auto"]
                },
                {
                    key: "visibility",
                    headerText: "Visibility",
                    styleClass: ["w100px", "center"]
                },
                {
                    key: "type",
                    headerText: "Type",
                    styleClass: ["w100px", "center"]
                },
                {
                    key: "state",
                    headerText: "State",
                    styleClass: ["w100px", "center"]
                }
            ],
            click: {
                fullRow: true,
                onClickHandler: this.editQuestionary
            }
        };


        const currentQuestionaryId = this.props.store.ux.questionary ? this.props.store.ux.questionary.id : null;

        return (
            <div className="default-page admin-view">

                <h1 className="page-title">Administration</h1>


                <section>
                    <h2>View mode</h2>
                    Change the view mode of this browser to something else.
    
                <div className="cols">
                        <div className="half">
                            <LabelButtons
                                text="View mode"
                                name="viewMode"
                                value={this.props.store.ux.viewMode}
                                onChange={this.changeMode}
                                choises={[
                                    {
                                        value: "FULL",
                                        text: "Single user"
                                    },
                                    {
                                        value: "KIOSK",
                                        text: "Kiosk mode"
                                    },
                                    {
                                        value: "MONITOR",
                                        text: "Only stats"
                                    }
                                ]} />



                        </div>
                        <div className="half">
                            {this.props.store.ux.viewMode === "FULL" && (
                                <p>This view mode is the default mode and expects that this device is used by a single person.
                             This means that each questionary can only be entered once.</p>
                            )}

                            {this.props.store.ux.viewMode === "KIOSK" && (
                                <p>This view mode expects that this device is used by a multiple people to answer questionaries.
                                     This means that after completing a questionary the user details are cleared and the service returns to
                             clear state.</p>
                            )}

                            {this.props.store.ux.viewMode === "MONITOR" && (
                                <p>This mode shows the statistics of the selected questionary in real time. It does not allow answering to questions at all.</p>
                            )}
                        </div>
                    </div>

                </section>


                <section>
                    <h2>Questionaries in the system</h2>

                    <p>Click a questionary to edit it</p>

                    <SimpleList list={questionaryList} options={listOptions} />

                    <div className="action-bar">

                        <AdminButton text="New Questionary" actionString="newQuestionary" onClick={this.transition} />
                    </div>
                </section>


                <section>
                    <h2>System Administration</h2>
                    <p>This part contains maintenance type settings and commands.</p>


                   
                   <AdminSnapshot />


                    <div className="action-bar">
                        
                        <button className="red" onClick={this.areYouSure}>Clear LocalStorage</button>
                    </div>
                </section>

                <ConfirmModal confirm={this.clearLocalStorage} cancel={this.cancelClear} open={this.showConfirm} />

            </div>
        )
    }
}
