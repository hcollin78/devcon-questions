import React from 'react';
import { observer, inject } from 'mobx-react';
import { observable } from 'mobx';

import qmachine from '../machines/QuestionaryMachine';

import QuestionItem from '../components/results/QuestionItem';

import '../styles/results-page.scss';

@inject("store", "viewMachine", "ux")
@observer
export default class ResultsView extends React.Component {

    @observable id = null;
    @observable questionary = null;

    constructor(props) {
        super(props);
        this.questionary = props.store.ux.questionary;
    }

    back = () => {
        // this.props.viewMachine.action("goToQuestionary");
        this.doMachineAction("backToList");
    };

    // This uses the custom QuestionMachine
    doMachineAction = (actionString, params = {}) => {
        const action = qmachine.action(actionString, this.props.store.ux.questionary, this.props.ux);
        if (action) this.props.store.qMachineActionHandler(action, params);

    };



    render() {

        if (this.questionary === null) {
            return (
                <div className="loading">
                    Loading...
                </div>
            )
        }

        const qlist = this.questionary.listQuestionsAsArray();
        const answers = this.props.store.myAnswers.filter(ans => ans.questionary.id === this.questionary.id);



        return (
            <div className="default-page results-view">
                <h1>{this.questionary.name}</h1>

                <p>{this.questionary.description}</p>

                <h2>Your answers</h2>
                <div className="answers">
                    {answers.map((ans, ind) => (
                        <div className="my-answer" key={ind}>
                            <span className="question">{ans.question.question}</span>
                            <span className="answer">{ans.answer.text}</span>
                        </div>
                    ))}
                </div>

                <div className="action-bar">
                    <button onClick={this.back} >Questionary List</button>
                </div>

            </div>
        )
    }
}