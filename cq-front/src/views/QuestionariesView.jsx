import React from 'react';
import { observer, inject } from 'mobx-react';
// import { action, observable } from 'mobx';
// import { Link } from 'react-router-dom';

import qmachine from '../machines/QuestionaryMachine';

import SimpleList from '../components/list/SimpleList';

@inject("store", "viewMachine", "ux")
@observer
export default class QuestionariesView extends React.Component {

    ux = null;

    constructor(props) {
        super(props);
        this.ux = props.store.ux;
        
    }

    setQuestionary = (questionary) => {
        this.doMachineAction("open", questionary);
    }

    doMachineAction = (actionString, questionary) => {
        const action = qmachine.action(actionString, questionary, this.props.ux);
        if(action) this.props.store.qMachineActionHandler(action, {questionary: questionary});
    };

    render() {

        const questionaryList = this.props.store.listQuestionariesAsArray().filter(qs => qs.state === "OPEN" || qs.state === "CLOSED");

        const listOptions = {
            columns: [
                {
                    key: "name",
                    headerText: "Name",
                    styleClass: ["w200px"]
                },
                {
                    key: "description",
                    headerText: "Description",
                    styleClass: ["auto"]
                },
                {
                    key: "visibility",
                    headerText: "Visibility",
                    styleClass: ["w100px", "center"]
                },
                {
                    key: "type",
                    headerText: "Type",
                    styleClass: ["w100px", "center"]
                },
                {
                    key: "state",
                    headerText: "State",
                    styleClass: ["w100px", "center"]
                }
            ],
            click: {
                fullRow: true,
                onClickHandler: this.setQuestionary
            }
        }

        return (
            <div className="default-page questionary-list-view">
                <h1 className="page-title">Questionaries View</h1>

                <p>Select the questionary you want to complete.</p>
            
                <SimpleList list={questionaryList} options={listOptions} selected={this.props.store.ux.questionary} />

            </div>
        )
    }
}
