import React from 'react';
import { observer, inject } from 'mobx-react';
import { observable, action } from 'mobx';

import AdminButton from './AdminButton';
import SimpleList from '../../components/list/SimpleList';
import LabelInput from '../../components/inputs/LabelInput';
import LabelSelect from '../../components/inputs/LabelSelect';
import LabelButtons from '../../components/inputs/LabelButtons';

import ConfirmModal from '../../components/modals/ConfirmModal';

import adminMachine from '../../machines/AdminMachine';
import AdminEditStore from './AdminStore';

import { Answer } from '../../models/Answer';

import {createAnswer, updateAnswer, removeAnswer} from '../../api/StoreApi';

import notificationService from '../../components/notifier/notificationService';

@inject("store")
@observer
export default class AnswerForm extends React.Component {

    @observable id = null;
    @observable text = "";
    @observable value = "";
    @observable nextQuestionId = "THE_END";

    @observable showConfirm = false;

    constructor(props) {
        super(props);

        if (props.answer !== null) {
            this.setStateFromData(AdminEditStore.answer);
        }

        this.handlers = {
            "createAnswer": (params, newState) => {
                
                createAnswer({
                    questionaryId: AdminEditStore.questionary.id,
                    questionId: AdminEditStore.question.id,
                    text: this.text,
                    value: this.value,
                    nextQuestionId: this.nextQuestionId
                }).then(reply => {
                    const na = AdminEditStore.question.newAnswer(reply.data);
                    AdminEditStore.setAnswer(na);
                    notificationService.success("New answer created: " + this.text);
                    return true;
                }).catch(error => {
                    notificationService.error(error.error);
                    console.log("ERROR", error);
                    return false;
                });
                return true;
            },
            "updateAnswer": (params, newState) => {
                
                updateAnswer({
                    questionaryId: AdminEditStore.questionary.id,
                    questionId: AdminEditStore.question.id,
                    id: this.id,
                    text: this.text,
                    value: this.value,
                    nextQuestionId: this.nextQuestionId
                }).then(reply => {
                    AdminEditStore.answer.update({
                        text: this.text,
                        value: this.value,
                        nextQuestionId: this.nextQuestionId,
                    });
                    notificationService.success("Answer updated: " + this.text);
                    return true;
                }).catch(error => {
                    notificationService.error(error.error);
                    console.log("ERROR", error);
                    return false;
                });
            },
            "clearSelectedAnswer": (params, ns) => {
                AdminEditStore.setAnswer(null);
                return true;
            },
            "removeAnswer": (params, newState) => {
                removeAnswer({
                    questionaryId: AdminEditStore.questionary.id,
                    questionId: AdminEditStore.question.id,
                    id: this.id,
                }).then(reply => {
                    AdminEditStore.question.removeAnswer(AdminEditStore.answer);
                    AdminEditStore.setAnswer(null);
                    notificationService.success("Answer removed: " + this.text);
                    return true;
                }).catch(error => {
                    notificationService.error(error.error);
                    console.log("ERROR", error);
                    return false;
                });
                return false;
            },
            "cancelQuestionary": (params, newState) => {
                return true;
            },
            "saveQuestionarySuccess": (params, newState) => {
                console.log("saving Questionary succesful");
                return true;
            }
        }
    }

    setStateFromData = (data) => {
        this.id = data.id;
        this.text = data.text;
        this.value = data.value;
        this.nextQuestionId = data.nextQuestionId ? data.nextQuestionId : "THE_END";
    };

    transition = (actionString, params) => {
        const newState = this.props.onTransition(actionString, params, this.handlers);
        console.log("Answer transition handler ", newState);
    }

    asyncTransition = (actionString, params) => {
        const newState = this.props.onAsyncTransition(actionString, params, this.handlers);
        console.log("Answer async transition handler ", newState);
    }

    onChange = (name, value) => {
        this[name] = value;
    };

    areYouSure = () => {
        this.showConfirm = true;
    }

    confirmDelete = () => {
        this.showConfirm = false;
        this.transition("delete", {});
    }

    cancelDelete = () => {
        this.showConfirm = false;
    }

    render() {

        const allQuestions = AdminEditStore.questionary.listQuestionsAsArray();

        const listQuestions = allQuestions.map(qs => { return { text: qs.question, value: qs.id }; });

        listQuestions.unshift({ text: "END OF QUESTIONARY", value: "THE_END" });

        return (
            <div className="default-page admin-view">
                <h1 className="page-title">Administration</h1>
                <div className="admin-form">
                    <h2>Answer</h2>


                    <div className="cols">
                        <div className="half">
                            <LabelInput
                                name="text"
                                value={this.text}
                                text="Answer text"
                                onChange={this.onChange} />

                        </div>
                        <div className="half">
                            <LabelInput
                                name="value"
                                value={this.value}
                                text="Answer value"
                                onChange={this.onChange} />


                            <LabelSelect
                                name="nextQuestionId"
                                value={this.nextQuestionId}
                                text="What is the next question?"
                                onChange={this.onChange}
                                choises={listQuestions} />

                        </div>
                    </div>




                    <div className="action-bar">
                        <div className="back-button-container">
                            <AdminButton text="Cancel" actionString="cancel" onClick={this.transition} />
                        </div>
                        {this.id === null && (
                            <React.Fragment>
                                <AdminButton text="Cancel" actionString="cancel" onClick={this.transition} />
                                <AdminButton text="Save new" actionString="save" onClick={this.asyncTransition}  className="ok" />
                            </React.Fragment>
                        )}
                        {this.id !== null && (
                            <React.Fragment>
                                {AdminEditStore.questionary.state !== "OPEN" && (
                                    <AdminButton text="Delete" actionString="delete" onClick={this.areYouSure}  className="red" />
                                )}
                                <AdminButton text="Update" actionString="update" onClick={this.asyncTransition}  className="ok" />
                            </React.Fragment>
                        )}

                    </div>
                </div>

                <ConfirmModal confirm={this.confirmDelete} cancel={this.cancelDelete} open={this.showConfirm}/>
            </div>
        );
    }

}
