import React from 'react';
import { observer, inject } from 'mobx-react';
import { observable, action } from 'mobx';

import { clearLocalState, getFullSnapShot, setFullSnapShot } from '../../api/StoreApi';

import notificationService from '../../components/notifier/notificationService';

import ConfirmModal from '../../components/modals/ConfirmModal';

@observer
export default class AdminSnapshot extends React.Component {

    @observable snapshotdata = "";
    @observable showConfirm = false;


    @action
    snapshotDataChanged = (e) => {
        this.snapshotdata = e.currentTarget.value;
        if(this.snapshotdata.length > 0) {
            try {
                const jsonData = JSON.parse(this.snapshotdata);
            } catch(err) {
                notificationService.warn("Snapshot is not valid JSON");
            }
            
            
        }
        
    }

    @action
    getSnapshot = () => {
        getFullSnapShot().then(action(reply => {
            this.snapshotdata = JSON.stringify(reply.data);
            notificationService.success("Snapshot retrieved!");
        }));
    }

    @action
    setSnapshot = () => {
        try {
            const jsonData = JSON.parse(this.snapshotdata);
            const keys = Object.keys(jsonData);
            if(
                keys.length === 3 || 
                keys.includes("users") ||
                keys.includes("questionaries") ||
                keys.includes("answers")
            ) {
                    setFullSnapShot(jsonData).then(rpl => {
                        console.log("New snapshot set as store! Please reset local Storage!");
                    });    
            } else {
                notificationService.error("Snapshot is not valid data!", keys);   
            }
            
        } catch( err ) {
            notificationService.error("Snapshot is not valid JSON!");
        }
        
        console.log("set snapshot");
        this.showConfirm = false;
    }

    @action
    areYouSure = () => {
        this.showConfirm = true;
    }

    @action
    cancelSave = () => {
        this.showConfirm = false;
    }

    render() {
        return (
            <React.Fragment>
                <h3>Backend database Snapshots</h3>

                <textarea className="snap-shot-area" name="snapshotarea" value={this.snapshotdata} onChange={this.snapshotDataChanged} />
                <div className="small-admin-bar">
                    {this.snapshotdata.length === 0 && (
                        <button onClick={this.getSnapshot}>Get Snapshot</button>
                    )}
                    {this.snapshotdata.length > 0 && (
                        <button onClick={this.areYouSure}>Set Snapshot</button>
                    )}
                </div>

                

                <ConfirmModal confirm={this.setSnapshot} cancel={this.cancelSave} open={this.showConfirm} />

            </React.Fragment>
        )
    }
}