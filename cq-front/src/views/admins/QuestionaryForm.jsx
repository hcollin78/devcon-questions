import React from 'react';
import { observer, inject } from 'mobx-react';
import { observable, action } from 'mobx';

import { createQuestionary, updateQuestionary, removeQuestionary } from '../../api/StoreApi';

import AdminButton from './AdminButton';


import SimpleList from '../../components/list/SimpleList';
import LabelInput from '../../components/inputs/LabelInput';
import LabelSelect from '../../components/inputs/LabelSelect';
import LabelButtons from '../../components/inputs/LabelButtons';

// import adminMachine from '../../machines/AdminMachine';
import AdminEditStore from './AdminStore';

import notificationService from '../../components/notifier/notificationService';
import ConfirmModal from '../../components/modals/ConfirmModal';


@inject("store", "viewMachine")
@observer
export default class QuestionaryForm extends React.Component {

    @observable id = null;
    @observable name = "";
    @observable description = "";
    @observable visibility = "PUBLIC";
    @observable type = "OPEN";
    @observable qState = "DRAFT";

    @observable questions = [];
    @observable startQuestion = null;

    @observable originalQuestionary = null;

    @observable saving = false;

    @observable showConfirm = false;


    constructor(props) {
        super(props);

        if (props.questionary !== null) {
            this.setStateFromData(AdminEditStore.questionary);
            this.originalQuestionary = props.store.getQuestionary(this.id);
        }

        this.handlers = {
            "createQuestionary": (params, newState) => {
                createQuestionary({
                    name: this.name,
                    description: this.description,
                    visibility: this.visibility,
                    type: this.type
                }).then(res => {
                    this.props.store.newQuestionary(res.data);
                    notificationService.success("New questionary created: "  + this.name);
                }).catch(error => {
                    notificationService.error(error.error);
                    console.log("ERROR", error);
                    return false;
                });    
                return true;
            },
            "updateQuestionary": (params, newState) => {

                const qs = AdminEditStore.questionary;
                
                const questionArr = this.originalQuestionary.listQuestionsAsArray()
                const startQuestion = this.startQuestion ? this.startQuestion.id : (questionArr.length > 0 ? questionArr[0].id: null);
                
                const data = {
                    name: this.name,
                    description: this.description,
                    visibility: this.visibility,
                    type: this.type,
                    state: this.qState,
                    id: this.id,
                    state: this.qState,
                    startQuestion: startQuestion
                };
                updateQuestionary(data).then(reply => {
                    notificationService.success("Questionary updated: \n" + this.name);
                    console.log(reply);
                }).catch(error => {
                    notificationService.error(error.error);
                    console.log("ERROR", error);
                    return false;
                });
                qs.update(data);
                
                

                return true;
            },
            "removeCurrentQuestionary": (params, newState) => {
                if (this.id && this.qState !== "OPEN") {

                    
                    removeQuestionary({id: this.id}).then(reply => {
                        notificationService.success("Questinary removed: " + this.name);
                        this.props.store.removeQuestionary(this.props.store.getQuestionary(this.id));
                    }).catch(error => {
                        notificationService.error(error.error);
                        console.log("ERROR", error);
                        return false;
                    });;

                    
                    return true;
                }
                return false;
            },
            "cancelQuestionary": (params, newState) => {
                return true;
            },
            "clearSelectedQuestion": (params, newState) => {
                AdminEditStore.setQuestion(null);
                return true;
            },
            "saveQuestionarySuccess": (params, newState) => {
                console.log("saving Questionary succesful");
                return true;
            }
        }
    }

    @action
    setStateFromData(data) {
        this.id = data.id;
        this.name = data.name;
        this.description = data.description;
        this.visibility = data.visibility;
        this.type = data.type;
        this.qState = data.state;

        this.questions = data.questions;
        this.startQuestion = data.startQuestion;
    }

    transition = (actionString, params) => {
        const newState = this.props.onTransition(actionString, params, this.handlers);
        console.log("Questionary transition handler ", newState);
    }

    asyncTransition = (actionString, params) => {
        const newState = this.props.onAsyncTransition(actionString, params, this.handlers);
        console.log("Questionary async transition handler ", newState);
    }

    onChange = (name, value) => {
        this[name] = value;
    }

    editQuestion = (question) => {
        console.log("edit question", question);
        AdminEditStore.setQuestion(question);
        this.transition("editQuestion", {});
    }

    areYouSure = () => {
        this.showConfirm = true;
    }

    confirmDelete = () => {
        this.showConfirm = false;
        this.transition("delete", {});
    }

    cancelDelete = () => {
        this.showConfirm = false;
    }



    render() {

        const questionListOptions = {
            columns: [
                {
                    key: "question",
                    headerText: "Question",
                    styleClass: ["w300px"]
                },
                {
                    key: "description",
                    headerText: "Description",
                    styleClass: ["auto"]
                },
                {
                    key: "answers",
                    headerText: "Answers",
                    styleClass: ["w100px", "center"],
                    type: "count"
                }
            ],
            click: {
                fullRow: true,
                onClickHandler: this.editQuestion
            }
        }


        
    
        const questionList = this.id ? this.originalQuestionary.listQuestionsAsArray() : [];

        const selectableQuestionList = questionList.map(qs => { return {value: qs.id, text: qs.question};});

        const startQuestionId  = this.startQuestion ? this.startQuestion : questionList.length > 0 ? questionList[0] : null;

        console.log("QUestions", this.questions, questionList, selectableQuestionList,startQuestionId);

        return (
            <div className="default-page admin-view">
                <h1 className="page-title">Administration</h1>
                <div className="admin-form">
                    <h2>Questionary</h2>

                    <div className="cols">
                        <div className="full">
                            <LabelButtons
                                name="qState"
                                value={this.qState}
                                text="State of the Questionary"
                                onChange={this.onChange}
                                choises={[
                                    {
                                        value: "DRAFT",
                                        text: "Draft"
                                    },
                                    {
                                        value: "OPEN",
                                        text: "Open"
                                    },
                                    {
                                        value: "CLOSED",
                                        text: "Closed"
                                    },
                                    {
                                        value: "ARCHIVED",
                                        text: "Archived"
                                    }]} />
                        </div>
                    </div>


                    <div className="cols">

                        <div className="half">

                            <LabelInput
                                name="name"
                                value={this.name}
                                text="Name"
                                onChange={this.onChange} />

                            <LabelInput
                                name="description"
                                value={this.description}
                                text="Description"
                                onChange={this.onChange} />

                        </div>

                        <div className="half">
                            <LabelSelect
                                name="visibility"
                                value={this.visibility}
                                text="Visibility to users"
                                onChange={this.onChange}
                                choises={[
                                    {
                                        value: "PUBLIC",
                                        text: "Visible to all"
                                    },
                                    {
                                        value: "USER",
                                        text: "Visible to all registered users"
                                    },
                                    {
                                        value: "INVITATION",
                                        text: "Visible to invited users"
                                    }]} />

                            <LabelSelect
                                name="type"
                                value={this.type}
                                text="Type of the Questionary"
                                onChange={this.onChange}
                                choises={[
                                    {
                                        value: "OPEN",
                                        text: "Open mode"
                                    },
                                    {
                                        value: "GAMESHOW",
                                        text: "Game Show mode"
                                    },
                                    {
                                        value: "TIMED",
                                        text: "Timed mode"
                                    }]} />

                            {selectableQuestionList && selectableQuestionList.length > 0 && (
                                <LabelSelect 
                                    name="startQuestion"
                                    value={startQuestionId}
                                    text="Starting Question"
                                    onChange={this.onChange}
                                    choises={selectableQuestionList} />
                            )}
                            
                            
                        </div>

                    </div>

                    {this.id && (
                        <React.Fragment>
                            <h2>Questions</h2>

                            <p>Click to edit a question or add new question from the action button.</p>

                            <SimpleList list={questionList} options={questionListOptions} />
                        </React.Fragment>
                    )}



                    <div className="action-bar">
                        <div className="back-button-container">
                            <AdminButton text="Cancel" actionString="cancel" onClick={this.transition} />
                        </div>
                        {!this.id && (
                            <React.Fragment>
                                <AdminButton text="Cancel" actionString="cancel" onClick={this.transition} />
                                <AdminButton text="Save New" actionString="save" onClick={this.asyncTransition} className="ok" />
                            </React.Fragment>
                        )}
                        {this.id && (
                            <React.Fragment>
                                {this.originalQuestionary.state !== "OPEN" && (
                                    <AdminButton text="Delete" actionString="delete" onClick={this.areYouSure}  className="red"/>
                                )}
                                <AdminButton text="Show Visualization" actionString="toggleVisualization" onClick={this.toggleVisualization} />
                                <AdminButton text="Add Question" actionString="newQuestion" onClick={this.transition} />
                                <AdminButton text="Update" actionString="update" onClick={this.asyncTransition}  className="ok"/>
                            </React.Fragment>
                        )}

                    </div>
                </div>
                
                <ConfirmModal confirm={this.confirmDelete} cancel={this.cancelDelete} open={this.showConfirm}/>
                
                
            </div>
        );
    }

}
