
import { observable, action, computed } from 'mobx';

class AdminContainer {

    @observable questionary = null;
    @observable question = null;
    @observable answer = null;

    @action
    setQuestionary(item) {
        this.questionary = item;
    }

    @action
    setQuestion(item) {
        this.question = item;
    }

    @action
    setAnswer(item) {
        this.answer = item;
    }

    @action
    clearAll() {
        this.questionary = null;
        this.question = null;
        this.answer = null;
    }
}

const AdminEditStore = new AdminContainer();
export default AdminEditStore;