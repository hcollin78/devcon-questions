import React from 'react';
import adminMachine from '../../machines/AdminMachine';
import {observer} from 'mobx-react';


const AdminButton = (props) => {

    const { text, actionString, onClick } = props;

    if (!adminMachine.isActionValid(actionString)) {
        return null;
    }

    function actionClick(e) {
        onClick(actionString);
    }

    

    return (
        <button className={props.className} onClick={actionClick}>{text}</button>

    )
};

export default observer(AdminButton);
