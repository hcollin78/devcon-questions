import React from 'react';
import { observer, inject } from 'mobx-react';
import { observable, action } from 'mobx';

import AdminButton from './AdminButton';
import SimpleList from '../../components/list/SimpleList';
import LabelInput from '../../components/inputs/LabelInput';
import LabelSelect from '../../components/inputs/LabelSelect';
import LabelButtons from '../../components/inputs/LabelButtons';

import ConfirmModal from '../../components/modals/ConfirmModal';

import adminMachine from '../../machines/AdminMachine';
import AdminEditStore from './AdminStore';

import { Question } from '../../models/Question';

import { createQuestion, updateQuestion, removeQuestion } from '../../api/StoreApi';

import notificationService from '../../components/notifier/notificationService';

@inject("store")
@observer
export default class QuestionForm extends React.Component {

    @observable id = null;
    @observable question = "";
    @observable description = "";
    @observable answers = [];

    @observable originalQuestion = null;

    @observable questionary = null;

    @observable showConfirm = false;

    constructor(props) {
        super(props);

        if (props.question !== null) {
            this.setStateFromData(AdminEditStore.question);
            this.questionary = AdminEditStore.questionary;
            this.originalQuestion = props.store.getQuestionary(this.id);
        }

        this.handlers = {
            "createQuestion": (params, newState) => {
                
                createQuestion({
                    questionaryId: AdminEditStore.questionary.id,
                    question: this.question,
                    description: this.description
                }).then(reply => {
                    const nq = AdminEditStore.questionary.newQuestion(reply.data);
                    AdminEditStore.setQuestion(nq);
                    notificationService.success("New question created: " + this.question);
                    return true;
                }).catch(error => {
                    notificationService.error(error.error);
                    console.log("ERROR", error);
                    return false;
                });
            
                return true;
            },
            "updateQuestion": (params, newState) => {
                
                updateQuestion({
                    questionaryId: AdminEditStore.questionary.id,
                    id: this.id,
                    question: this.question,
                    description: this.description
                }).then(reply => {
                    AdminEditStore.question.update(reply.data);
                    notificationService.success("Question updated: " + this.question);
                    return true;
                }).catch(error => {
                    notificationService.error(error.error);
                    console.log("ERROR", error);
                    return false;
                });
                return true;
            },
            "removeQuestion": (params, newState) => {
                removeQuestion({
                    questionaryId: AdminEditStore.questionary.id,
                    id: this.id
                }).then(reply => {
                    AdminEditStore.questionary.removeQuestion(AdminEditStore.question);
                    AdminEditStore.setQuestion(null);
                    notificationService.success("Question deleted: " + this.question);
                    return true;
                }).catch(error => {
                    notificationService.error(error.error);
                    console.log("ERROR", error);
                    return false;
                })
            },
            "cancelQuestionary": (params, newState) => {
                return true;
            },
            "saveQuestionarySuccess": (params, newState) => {
                console.log("saving Questionary succesful");
                return true;
            }
        }
    }

    setStateFromData = (data) => {
        this.id = data.id;
        this.question = data.question;
        this.description = data.description ? data.description : "";
        this.answers = data.answers ? data.answers : [];
    };

    transition = (actionString, params) => {
        const newState = this.props.onTransition(actionString, params, this.handlers);
        console.log("Question transition handler ", newState);
    }

    asyncTransition = (actionString, params) => {
        const newState = this.props.onAsyncTransition(actionString, params, this.handlers);
        console.log("Question async transition handler ", newState);
    }

    onChange = (name, value) => {
        this[name] = value;
    };

    editAnswer = (answer) => {
        AdminEditStore.setAnswer(answer);
        this.transition("editAnswer", {});
    }

    newAnswer = () => {
        AdminEditStore.setAnswer(null);
        this.transition("newAnswer", {});
    }

    areYouSure = () => {
        this.showConfirm = true;
    }

    confirmDelete = () => {
        this.showConfirm = false;
        this.transition("delete", {});
    }

    cancelDelete = () => {
        this.showConfirm = false;
    }

    render() {


        const answerListOptions = {
            columns: [
                {
                    key: "text",
                    headerText: "Answer Text",
                    styleClass: ["auto"]
                },
                {
                    key: "value",
                    headerText: "Value",
                    styleClass: ["w100px", "center"]
                },
                {
                    key: "nextQuestionId",
                    headerText: "Next Question",
                    styleClass: ["w200px", "center"],
                    func: (val) => {
                        if(val === "THE_END") {
                            return "THE END";
                        }
                        const qs = AdminEditStore.questionary;
                        const q = qs.getQuestionById(val);
                        return q ? q.question : '-';
                    }
                }
            ],
            click: {
                fullRow: true,
                onClickHandler: this.editAnswer
            }
        };

        const answerList = this.id ? AdminEditStore.question.answers : [];

        return (
            <div className="default-page admin-view">
                <h1 className="page-title">Administration</h1>
                <div className="admin-form">
                    <h2>Question</h2>


                    <div className="cols">
                        <div className="half">
                            <LabelInput
                                name="question"
                                value={this.question}
                                text="Question text"
                                onChange={this.onChange} />

                        </div>
                        <div className="half">
                            <LabelInput
                                name="description"
                                value={this.description}
                                text="Description"
                                onChange={this.onChange} />

                        </div>
                    </div>

                    {this.id && (
                        <React.Fragment>
                            <h2>Answers</h2>

                            <p>Click to edit an answer or add a new answer from the action button below.</p>

                            <SimpleList list={answerList} options={answerListOptions} />
                        </React.Fragment>
                    )}



                    <div className="action-bar">
                    <div className="back-button-container">
                            <AdminButton text="Cancel" actionString="cancel" onClick={this.transition} />
                        </div>
                        {!this.id && (
                            <React.Fragment>
                                <AdminButton text="Cancel" actionString="cancel" onClick={this.transition} />
                                <AdminButton text="Save new" actionString="save" onClick={this.asyncTransition}  className="ok"/>
                            </React.Fragment>
                        )}
                        {this.id && (
                            <React.Fragment>
                                {AdminEditStore.questionary.state !== "OPEN" && (
                                    <AdminButton text="Delete" actionString="delete" onClick={this.areYouSure}  className="red" />
                                )}
                                <AdminButton text="Add Answer" actionString="newAnswer" onClick={this.newAnswer} />
                                <AdminButton text="Update" actionString="update" onClick={this.asyncTransition}  className="ok" />
                            </React.Fragment>
                        )}

                    </div>
                </div>

               <ConfirmModal confirm={this.confirmDelete} cancel={this.cancelDelete} open={this.showConfirm}/>
                 
            </div>
        );
    }

}
