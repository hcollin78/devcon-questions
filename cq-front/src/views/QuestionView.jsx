import React from 'react';
import { observer, inject } from 'mobx-react';

import qmachine from '../machines/QuestionaryMachine';


import '../styles/question-page.scss';

@inject("store", "viewMachine", "ux")
@observer
export default class QuestionView extends React.Component {

    goRoot = () => {
        this.props.store.ux.setViewState("QUESTIONARY");
    };

    handleAnswer = (answer) => {
        console.log("Clicked answer", answer, this.props.store.ux);
        
        if(answer.isLastQuestion()) {
            this.doMachineAction("selectLastAnswer", answer);
        } else {
            this.doMachineAction("selectAnswer", answer);
        }

    };

     // This uses the custom QuestionMachine
     doMachineAction = (actionString, params={}) => {
        const action = qmachine.action(actionString, this.props.store.ux.questionary, this.props.ux);
        if(action) this.props.store.qMachineActionHandler(action, params);

    };

    render() {

        console.log("Question Render", this.props.store.ux);

        const questionary = this.props.store.ux.questionary;
        const question = this.props.store.ux.question;

        if(question===null) {
            this.props.viewMachine.action("endQuestionary");
            return null;
        }

        
        const currentAnswer = this.props.store.getMyAnswerTo();


        return (
            <div className="question-page">
                <h2 className="questionary">{questionary.name}</h2>
                <h1 className="question">{question.question}</h1>

                <div className="choises">
                    {question.answers.map(answer => (
                        <AnswerItem key={answer.id} answer={answer} onClick={this.handleAnswer} selected={currentAnswer ? currentAnswer.answer.id === answer.id: false} />
                    ))}
                </div>


            </div>
        )
    }
}

const AnswerItem = ({ answer, onClick, selected, ...others }) => {

    function handleClick() {
        onClick(answer);
    }

    const classes = [
        "answer-item",
        selected ? "selected" : ""
    ].join(" ");

    return (
        <div className={classes} onClick={handleClick} data={answer}>
            {answer.text}
        </div>
    );

}