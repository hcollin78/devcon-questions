import React from 'react';
import { observer, inject } from 'mobx-react';
import { observable, action, transaction } from 'mobx';

import { confirmSudoAdminPassword } from '../api/UserApi';

import Numpad from '../components/numpad/Numpad';

import notificationService from '../components/notifier/notificationService';

import "../styles/keypad.scss";

@inject("store", "viewMachine")
@observer
export default class LoginView extends React.Component {


    @observable passwd = "";
    @observable checking = false;
    

    @observable loginSuccesful = null;
    @observable errorMsg = false;

    loginAsUser = () => {

    };

    loginAsAdmin = (pin) => {
        this.checking = true;
        confirmSudoAdminPassword(pin).then(res => {
            
            this.checking = false;
            notificationService.success("Admin mode enabled");
            this.props.viewMachine.action("submit");
            
            
        }).catch(err => {

            notificationService.error("Invalid pin");
            this.checking = false;
            this.errorMsg = "Invalid Pin";
        });
    };

    cancelLogin = () => {
        this.props.viewMachine.action("cancel", {});
    }


    render() {

        return (
            <div className="default-page login-view">

                <Numpad onConfirm={this.loginAsAdmin} onCancel={this.cancelLogin} disabled={this.checking} helpText="Enter pin" errorText={this.errorMsg}/>

            </div>
        );
    }
}