import React from 'react';
import { observer, inject } from 'mobx-react';
import { observable, action, computed } from 'mobx';

import {getStatisticalAnswers, listenForStatisticalUpdates} from '../api/AnswerApi';

import statisticsStore from '../models/StatisticsStore';
import QuestionItem from '../components/results/QuestionItem';

@inject("store", "viewMachine")
@observer
export default class StatisticsView extends React.Component {

    @observable store = statisticsStore;

    constructor(props) {
        super(props);
        this.store.startStore(this.props.store.ux.questionary);
    }


    componentWillUnmount() {
        this.store.stopUpdates();
    }

    render() {

        if(this.store.loading) {
            
            return null;
        }
       
        const qlist = this.store.getQuestions();

        const answersToThis = this.store.answers.filter(a => {
            if(typeof a.questionary === "string") {
                return a.questionary === this.props.store.ux.questionary.id    
            }
            return a.questionary.id === this.props.store.ux.questionary.id
        }).length;
       
        return (
            <div className="default-page">
                <h1>{this.store.questionary.name}</h1>

                <p>Total questions answered: {this.store.answers.length}</p>

                <div className="question-items">
                {qlist.map((question, ind) => (
                    <QuestionItem question={question} key={ind} answers={this.store.answers} />
                ))}
            
                </div>
                
            </div>
        );
    }
}
