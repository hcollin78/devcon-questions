import { Machine } from 'xstate';
import { observable } from 'mobx';

export const AdminMachineJSON = {
    initial: "ROOT",
    key: "AdminMachine",
    states: {
        ROOT: {
            on: {
                editQuestionary: 'QUESTIONARY',
                newQuestionary: {'QUESTIONARY': { actions: ['clearEditData'] } },
                editQuestion: 'QUESTION',
                newQuestion: 'QUESTION',
                changeMode: { 'ROOT': { actions: ['changeMode', 'saveStateToLocalStorage'] } },
            }
        },
        QUESTIONARY: {
            on: {
                save: {
                    'SAVING': {
                        actions: ['createQuestionary', 'saveStateToLocalStorage']
                    }
                },
                update: {
                    'SAVING': {
                        actions: ['updateQuestionary', 'saveStateToLocalStorage']
                    }
                },
                cancel: 'ROOT',
                delete: {'ROOT': { actions: ['removeCurrentQuestionary'] } },
                editQuestion: 'QUESTION',
                newQuestion: {'QUESTION': { actions: ['clearSelectedQuestion'] } },
                clearForm: {
                    'QUESTIONARY': {
                        actions: ['clearQuestionaryForm']
                    }
                },
                toggleVisualization: 'QUESTIONARY'
            }
        },
        QUESTION: {
            on: {
                save: {'QUESTIONARY': { actions: ['createQuestion','saveStateToLocalStorage'] } },
                update: {'QUESTIONARY': { actions: ['updateQuestion','saveStateToLocalStorage'] } },
                cancel: {'QUESTIONARY': { actions: ['clearSelectedQuestion'] } },
                delete: {'QUESTIONARY': { actions: ['removeQuestion','saveStateToLocalStorage'] } },
                newAnswer: 'ANSWER',
                editAnswer: 'ANSWER'
            }
        },
        ANSWER: {
            on: {
                save: {'QUESTION': { actions: ['createAnswer','saveStateToLocalStorage'] } },
                update: {'QUESTION': { actions: ['updateAnswer','saveStateToLocalStorage'] } },
                cancel: {'QUESTION': { actions: ['clearSelectedAnswer'] } },
                delete: {'QUESTION': { actions: ['removeAnswer','saveStateToLocalStorage'] } }
            }
        },
        SAVING: {
            on: {
                resolve: { 'ROOT': { actions: ['changeView'] } },
                reject: { 'ROOT': { actions: ['changeView'] } }
            }
        }
    }
};

// resolve: { 'ROOT': { actions: ['changeView'] } },

export class AdminMachine {

    @observable currentState;

    constructor() {
        this.machine = Machine(AdminMachineJSON);
        this.currentState = this.machine.initialState.value;
        this.defaultHandlers = {};
    }

    setDefaultHandlers(defaultHandlers) {
        this.defaultHandlers = defaultHandlers;
    }

    transition(actionString, actionHandler, params) {
        console.log("AdminMachine.transition:", this.currentState, actionString, params)
        const newState = this.machine.transition(this.currentState, actionString);
        newState.actions.forEach(act => {
            if(actionHandler[act] !== undefined) {
                actionHandler[act](params, newState);
            } else {
                if(this.defaultHandlers[act] !== undefined) {
                    this.defaultHandlers[act](params, newState);
                } else {
                    console.warn("adminMachine.transition: No handler found for action: ", actionString);
                }
                
            }
        });
        this.currentState = newState.value;
        return newState;
    }

    asyncTransition(actionString, actionHandler, params) {

        console.log("AdminMachine.asyncTransition:", this.currentState, actionString, params)
        const newState = this.machine.transition(this.currentState, actionString);
        const promises = [];
        newState.actions.forEach(act => {
            if(actionHandler[act] !== undefined) {
                promises.push(actionHandler[act](params, newState));
            } else {
                if(this.defaultHandlers[act] !== undefined) {
                    promises.push(this.defaultHandlers[act](params, newState));
                } else {
                    console.warn("adminMachine.asyncTransition: No handler found for action: ", actionString);
                }
                
            }
        });
        this.currentState = newState.value;
        Promise.all(promises).then(res => {
            const lastState = this.transition("resolve", actionHandler, { params: params, results: res, origAction: actionString });
            this.currentState = lastState.value;
        }).catch(err => {
            const lastState = this.transition("reject", actionHandler, { params: params, error: err, origAction: actionString });
            this.currentState = lastState.value;
        });
        return newState;

    }

    isActionValid(actionString) {
        return this.machine.getState(this.currentState).events.includes(actionString);
    }
}

const adminMachine = new AdminMachine();
export default adminMachine;