
// The state depends on following things
//  - type of the questionary [OPEN, GAMESHOW, TIMED]
//  - mode of the view [FULL, KIOSK, MONITOR, GAMEHOST]
//  - state of the questionary [DRAFT, OPEN, CLOSED, ARCHIVED]
//  - state of the view [QUESTIONARY, QUESTION, MYRESULTS, FULLRESULTS]

// This is a custom machine (not using xstate) as the requirements are much more specific

// Order is [Questionary State][Questinary Type][View Mode][View State]

class QuestionaryMachine {

    constructor(stateConfig={}) {
        this.states = stateConfig;
    }

    getActions(questionary, ux) {
        const keys = [questionary.state, questionary.type, ux.mode, ux.state];
        const results = this._travelMachine(this.states, keys, 0);
        return results;
    }

    action(actionString, questionary, ux) {
        const options = this.getActions(questionary, ux);
        if(options && options.actions[actionString]) {
            return options.actions[actionString];
        }
        return false;
    }

    _travelMachine(options, keys, depth) {
        const key = keys[depth];
        
        // We have reached the end of the machine tree, return current depth;
        if(key === undefined) {
            return options;
        }

        
        // This is an invalid key, no options are available.
        const nextOptions = options[key];
        if(nextOptions === undefined) {
            return false;
        }

        // Otherwise move deeper into the machine tree
        return this._travelMachine(nextOptions, keys, depth + 1);
    }
}

const states = {

    // DRAFT QUESTIONARY
    "DRAFT": {
        // Nothing can be done for questionaries that are still in draft mode.        
    },

    // OPEN QUESTIONARY
    "OPEN": {

        // OPEN QUESTIONARY / OPEN TYPE 
        "OPEN": {

            // OPEN QUESTIONARY / OPEN TYPE / FULL MODE
            "FULL": {

                // OPEN QUESTIONARY / OPEN TYPE / FULL MODE / QUESTIONARIES PAGE
                "QUESTIONARIES":{
                    actions:{
                        "open": {
                            do: ["selectQuestionary"],
                            
                            viewState: [
                                {
                                    condition: "userHasCompletedQuestionary",
                                    viewState: "MYRESULTS"
                                },
                                {
                                    viewState:  "QUESTIONARY"
                                }
                            ]
                        },
                        "showResults": {
                            do: ["selectQuestionary"],
                            viewState: "MYRESULTS"
                        }
                    }
                },
                
                // OPEN QUESTIONARY / OPEN TYPE / FULL MODE / QUESTIONARY PAGE
                "QUESTIONARY":{
                    actions:{
                        "start": {
                            do: ["testAction", "setFirstQuestionInQuestionaryAsActive"],
                            viewState: "QUESTION",
                            
                        }
                    }
                },

                // OPEN QUESTIONARY / OPEN TYPE / FULL MODE / QUESTION PAGE
                "QUESTION":{
                    actions:{
                        "selectAnswer": {
                            do: ["addOrUpdateMyAnswer", "selectNextQuestion"]
                        },
                        "selectLastAnswer": {
                            do: ["addOrUpdateMyAnswer", "completeQuestionary", "clearSelectedQuestion"],
                            viewState: "MYRESULTS",
                            post: ["saveToLocal"]
                        }
                    }
                },

                // OPEN QUESTIONARY / OPEN TYPE / FULL MODE / MYRESULTS PAGE
                "MYRESULTS":{
                    actions:{
                        "backToList": {
                            viewState: "QUESTIONARIES",
                            post: ["clearSelectedQuestionary"]
                            
                        }
                    }
                },

                // OPEN QUESTIONARY / OPEN TYPE / FULL MODE / FULLRESULTS PAGE ! NOT AVAILABLE (UNTIL CLOSED!)
                
            },

            // OPEN QUESTIONARY / OPEN TYPE / KIOSK MODE
            "KIOSK": {

                // OPEN QUESTIONARY / OPEN TYPE / KIOSK MODE / QUESTIONARIES PAGE
                "QUESTIONARIES":{
                    actions:{
                        "open": {
                            do: ["selectQuestionary", "clearPreviousAnswers", "createNewKioskUser"],
                            viewState: "QUESTIONARY"
                        }
                    }
                },

                // OPEN QUESTIONARY / OPEN TYPE / KIOSK MODE / QUESTIONARY PAGE
                "QUESTIONARY":{
                    actions:{
                        "start": {
                            do: ["setFirstQuestionInQuestionaryAsActive"],
                            viewState: "QUESTION"
                        }
                    }
                },

                // OPEN QUESTIONARY / OPEN TYPE / KIOSK MODE / QUESTION PAGE
                "QUESTION":{
                    actions:{
                        "selectAnswer": {
                            do: ["addOrUpdateMyAnswer", "selectNextQuestion"]
                        },
                        "selectLastAnswer": {
                            do: ["addOrUpdateMyAnswer", "completeQuestionary", "clearSelectedQuestion"],
                            viewState: "MYRESULTS"                            
                        }
                    }
                },

                // OPEN QUESTIONARY / OPEN TYPE / KIOSK MODE / MYRESULTS PAGE
                "MYRESULTS":{
                    actions:{
                        "backToList": {
                            viewState: "QUESTIONARIES",
                            post: ["clearSelectedQuestionary"]
                            
                        }
                    }
                }

                // OPEN QUESTIONARY / OPEN TYPE / KIOSK MODE / FULLRESULTS PAGE ! NOT AVAILABLE UNTIL CLOSED
                
            },

            // OPEN QUESTIONARY / OPEN TYPE / MONITOR MODE 
            "MONITOR": {

                // OPEN QUESTIONARY / OPEN TYPE / MONITOR MODE / QUESTIONARIES PAGE
                "QUESTIONARIES":{
                    actions:{
                        "open": {
                            do: ["selectQuestionary"],
                            viewState: "STATISTICS"
                        }
                    }
                },

                // OPEN QUESTIONARY / OPEN TYPE / MONITOR MODE / QUESTIONARY PAGE ! NOT ALLOWED
                // OPEN QUESTIONARY / OPEN TYPE / MONITOR MODE / QUESTION PAGE ! NOT ALLOWED
                // OPEN QUESTIONARY / OPEN TYPE / MONITOR MODE / MYRESULTS PAGE ! NOT ALLOWED
                
                // OPEN QUESTIONARY / OPEN TYPE / MONITOR MODE / FULLRESULT PAGE 
                "FULLRESULTS":{
                    actions:{
                        "backToList": {
                            viewState: "QUESTIONARIES",
                            post: ["clearSelectedQuestionary"]
                            
                        }
                    }
                }
            },
        },

        // OPEN QUESTIONARY / GAMESHOT TYPE
        "GAMESHOW": {

            // OPEN QUESTIONARY / GAMESHOT TYPE / FULL MODE 
            "FULL": {

                // OPEN QUESTIONARY / GAMESHOT TYPE / FULL MODE / QUESTIONARIES PAGE
                "QUESTIONARIES":{
                    actions:{
                        "open": true
                    }
                },
                
                // OPEN QUESTIONARY / GAMESHOT TYPE / FULL MODE / QUESTIONARY PAGE
                "QUESTIONARY":{
                    actions:{
                        "start": true
                    }
                },

                // OPEN QUESTIONARY / GAMESHOT TYPE / FULL MODE / QUESTION PAGE
                "QUESTION":{
                    actions:{
                        "select": true
                    }
                },

                // OPEN QUESTIONARY / GAMESHOT TYPE / FULL MODE / MYRESULTS PAGE
                "MYRESULTS":{
                    actions:{
                        "confirm": true
                    }
                }

                // OPEN QUESTIONARY / GAMESHOT TYPE / FULL MODE / MYRESULTS PAGE ! NOT AVAILABLE
            },

            // OPEN QUESTIONARY / GAMESHOT TYPE / KIOSK MODE ! NOT AVAILABLE
            
            // OPEN QUESTIONARY / GAMESHOT TYPE / MONITOR MODE 
            "MONITOR": {

                // OPEN QUESTIONARY / GAMESHOT TYPE / MONITOR MODE / QUESTIONARIES PAGE 
                "QUESTIONARIES":{
                    actions:{
                        "open": true
                    }
                },
                
                // OPEN QUESTIONARY / GAMESHOT TYPE / MONITOR MODE / QUESTIONARY PAGE 
                "QUESTIONARY":{
                    actions:{
                        "start": true
                    }
                },

                // OPEN QUESTIONARY / GAMESHOT TYPE / MONITOR MODE / QUESTION PAGE 
                "QUESTION":{
                    actions:{
                        "select": true
                    }
                },

                // OPEN QUESTIONARY / GAMESHOT TYPE / MONITOR MODE / MYRESULTS PAGE ! NOT AVAILABLE
                
                // OPEN QUESTIONARY / GAMESHOT TYPE / MONITOR MODE / FULLRESULTS PAGE
                "FULLRESULTS":{
                    actions:{}
                }
            },

            "GAMEHOST": {
                "QUESTIONARIES":{
                    actions:{
                        "open": true
                    }
                },
                "QUESTIONARY":{
                    actions:{
                        "start": true
                    }
                },
                "QUESTION":{
                    actions:{
                        "select": true
                    }
                },
                "MYRESULTS":{
                    actions:{
                        "confirm": true
                    }
                },
                "FULLRESULTS":{
                    actions:{}
                }
            }
        },

        "TIMED": {
            "FULL": {
                "QUESTIONARIES":{
                    actions:{
                        "open": true
                    }
                },
                "QUESTIONARY":{
                    actions:{
                        "start": true
                    }
                },
                "QUESTION":{
                    actions:{
                        "select": true
                    }
                },
                "MYRESULTS":{
                    actions:{
                        "confirm": true
                    }
                },
                "FULLRESULTS":{
                    actions:{}
                }
            },

            "KIOSK": {
                "QUESTIONARIES":{
                    actions:{
                        "open": true
                    }
                },
                "QUESTIONARY":{
                    actions:{
                        "start": true
                    }
                },
                "QUESTION":{
                    actions:{
                        "select": true
                    }
                },
                "MYRESULTS":{
                    actions:{
                        "confirm": true
                    }
                },
                "FULLRESULTS":{
                    actions:{}
                }
            },

            "MONITOR": {
                "QUESTIONARIES":{
                    actions:{
                        "open": true
                    }
                },
                "QUESTIONARY":{
                    actions:{
                        "start": true
                    }
                },
                "QUESTION":{
                    actions:{
                        "select": true
                    }
                },
                "MYRESULTS":{
                    actions:{
                        "confirm": true
                    }
                },
                "FULLRESULTS":{
                    actions:{}
                }
            },

            "GAMEHOST": {
                "QUESTIONARIES":{
                    actions:{
                        "open": true
                    }
                },
                "QUESTIONARY":{
                    actions:{
                        "start": true
                    }
                },
                "QUESTION":{
                    actions:{
                        "select": true
                    }
                },
                "MYRESULTS":{
                    actions:{
                        "confirm": true
                    }
                },
                "FULLRESULTS":{
                    actions:{}
                }
            }
        }
    },
    
    // CLOSED QUESTIONARY
    "CLOSED": {

        // CLOSED QUESTIONARY / OPEN TYPE 
        "OPEN": {
            
            // CLOSED QUESTIONARY / OPEN TYPE / FULL MODE
            "FULL": {

                // CLOSED QUESTIONARY / OPEN TYPE / FULL MODE / QUESTIONARIES PAGE
                "QUESTIONARIES":{
                    actions:{
                        "open": true
                    }
                },
                "QUESTIONARY":{
                    actions:{
                        "results": true
                    }
                },
                "FULLRESULTS":{
                    actions:{}
                }
            },
            "KIOSK": {
                "QUESTIONARIES":{
                    actions:{
                        "open": true
                    }
                },
                "QUESTIONARY":{
                    actions:{
                        "results": true
                    }
                },
                "FULLRESULTS":{
                    actions:{}
                }
            },  
            "MONITOR": {
                "QUESTIONARIES":{
                    actions:{
                        "open": true
                    }
                },
                "QUESTIONARY":{
                    actions:{
                        "results": true
                    }
                },
                "FULLRESULTS":{
                    actions:{}
                }
            },
            "GAMEHOST": {
                "QUESTIONARIES":{
                    actions:{
                        "open": true
                    }
                },
                "QUESTIONARY":{
                    actions:{
                        "results": true
                    }
                },
                "FULLRESULTS":{
                    actions:{}
                }
            }
        },
        "GAMESHOW": {
            "FULL": {
                "QUESTIONARIES":{
                    actions:{
                        "open": true
                    }
                },
                "QUESTIONARY":{
                    actions:{
                        "results": true
                    }
                },
                "FULLRESULTS":{
                    actions:{}
                }
            },
            "KIOSK": {
                "QUESTIONARIES":{
                    actions:{
                        "open": true
                    }
                },
                "QUESTIONARY":{
                    actions:{
                        "results": true
                    }
                },
                "FULLRESULTS":{
                    actions:{}
                }
            },
            "MONITOR": {
                "QUESTIONARIES":{
                    actions:{
                        "open": true
                    }
                },
                "QUESTIONARY":{
                    actions:{
                        "results": true
                    }
                },
                "FULLRESULTS":{
                    actions:{}
                }
            },
            "GAMEHOST": {
                "QUESTIONARIES":{
                    actions:{
                        "open": true
                    }
                },
                "QUESTIONARY":{
                    actions:{
                        "results": true
                    }
                },
                "FULLRESULTS":{
                    actions:{}
                }
            }
        },
        "TIMED": {
            "FULL": {
                "QUESTIONARIES":{
                    actions:{
                        "open": true
                    }
                },
                "QUESTIONARY":{
                    actions:{
                        "results": true
                    }
                },
                "FULLRESULTS":{
                    actions:{}
                }
            },
            "KIOSK": {
                "QUESTIONARIES":{
                    actions:{
                        "open": true
                    }
                },
                "QUESTIONARY":{
                    actions:{
                        "results": true
                    }
                },
                "FULLRESULTS":{
                    actions:{}
                }
            },
            "MONITOR": {
                "QUESTIONARIES":{
                    actions:{
                        "open": true
                    }
                },
                "QUESTIONARY":{
                    actions:{
                        "results": true
                    }
                },
                "FULLRESULTS":{
                    actions:{}
                }
            },
            "GAMEHOST": {
                "QUESTIONARIES":{
                    actions:{
                        "open": true
                    }
                },
                "QUESTIONARY":{
                    actions:{
                        "results": true
                    }
                },
                "FULLRESULTS":{
                    actions:{}
                }
            }
        }
    },
    "ARCHIVED": {
        // Nothing can be done for questionaries that have been archived.
    }
};

const qmachine = new QuestionaryMachine(states);
export default qmachine;