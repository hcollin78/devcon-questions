import { Machine } from 'xstate';

import statisticsMachine from './StatisticsMachine.json';
import defaultMachine from './DefaultMachine.json';
import kioskMachine from './KioskMachine.json';

/**
 *  Adds the sites login functionality and capability to mode state machine
 * */
function hasLoginAvailable(machineConfig) {
    
    const initState = machineConfig.initial;

    machineConfig.states[initState]['on']['goToLogin'] = 'LOGIN'
    machineConfig.states[initState]['on']['goToLogout'] = initState;
    machineConfig.states['LOGIN'] = {
        on: {
            submit: {
                'ADMIN': { 
                    actions: ['sudoUser']
                }
            },
            'cancel': {}
        }
    };

    machineConfig.states['ADMIN'] = {
        on: {
            saveChanges: initState,
            cancelChanges: initState,
            goToLogout: {}
        }
    
    };

    machineConfig.states['LOGIN']['on']['cancel'][initState] = { 
        actions: ['deSudoUser']
    }

    machineConfig.states['ADMIN']['on']['goToLogout'][initState] = { 
        actions: ['deSudoUser']
    }

    return machineConfig;
}

const statsModeJson = hasLoginAvailable(statisticsMachine);
const kioskModeJson = hasLoginAvailable(kioskMachine);
const singleModeJson = hasLoginAvailable(defaultMachine);


export default class ViewMachine {
    
    constructor(store) {
        // console.log("MachineJSon", machineJson);
        this.store = store;
        // this.machine = Machine(machineJson);

        this.machines = {
            "KIOSK": Machine(kioskModeJson),
            "FULL": Machine(singleModeJson),
            "MONITOR": Machine(statsModeJson),
        };

        const currentState = this.store.ux.viewState;
        const currentMode = this.store.ux.viewMode;
        // console.log("\n\n", currentState, currentMode);
    }

    /**
     * Function for doing synchronous state machine driven actions. If the action is unknown nothing happens.
     * 
     * @param {string} action 
     * @param {*} params 
     */
    action(action, params=false) {
        // console.log("DO ACTION", action);
        const currentState = this.store.ux.viewState;
        
        
        const machine = this._getCurrentMachine();
        const newState = machine.transition(currentState, action, (params ? params : this.store));
        newState.actions.forEach(act => {
            const ret = this.store.doAction(act, params, newState) 
            if(!ret) {
                console.error("ACTION '" + action + "' FAILED", ret);                
            }
        });
        this.store.ux.setViewState(newState.value);
        console.log("\tNEW STATE IS:", this.store.ux.viewState);
    }

    adminAction(action, params) {
        if(!this.store.currentUserIsAdmin()) {
            return;
        }
        this.action(action, params);
    }

    /**
     * Function for doing asynchronous state machine driven actions.
     * 
     * @param {*} action 
     * @param {*} params 
     */
    asyncAction(action, params=false) {
        console.log("DO ASYNC ACTION", action);
        const currentState = this.store.ux.viewState;
        const machine = this._getCurrentMachine();
        const newState = machine.transition(currentState, action, (params ? params : this.store));

        const promises = [];
        newState.actions.forEach(act => {
            promises.push(this.store.doAction(act, params, newState)) 
        });
        this.store.ux.setViewState(newState.value);
        // console.log("\tNEW STATE IS:", this.store.ux.viewState);
        
        Promise.all(promises).then(res => {
            // console.log("\tResolve", action, " res:", res);
            this.action("resolve", {params: params, resolveResults: res, currentStore: this.store});
        }).catch(err => {
            // console.warn("\tReject", action, " err:", err);
            this.action("reject", err);
        });
    }

    /**
     * Return all actions available in target State (current state if omitted)
     */
    getActions(forState=false) {
        const machine = this._getCurrentMachine();
        const state = machine.getState(forState !== false ? forState : this.store.ux.viewState);
        // console.log("STATE", state, machine, this.store.ux.viewState, this.store.ux.viewMode);
        if(state === undefined) {
            return [];
        }
        return state.events;
    }

    /**
     * Sets the current viewState to Modes default if the current mode does not support the current state.
     * This can happen when loading state from the localStorage
     */
    correctStateIfNecessary() {
        const machine = this._getCurrentMachine();
        const state = machine.getState(this.store.ux.viewState);
        if(!state) {
            this.store.ux.setViewState(machine.initial);
        }
    }

    /**
     * Does state (current or target) has the specified action available
     * @param {*} action 
     * @param {*} forState 
     */
    hasAction(action, forState=false) {
        return this.getActions(forState).includes(action);
    }

    _getCurrentMachine() {
        const currentMode = this.store.ux.viewMode;
        return this.machines[currentMode];
    }

}



