import { types, views } from 'mobx-state-tree';


export const User = types.model({
    id: types.identifier(types.string),
    email: types.maybe(types.string),
    name: types.maybe(types.string),
    role: types.optional(types.enumeration(["ANONYMOUS", "ADMIN", "LOCALUSER", "KIOSK", "USER"]), "ANONYMOUS"),
    completedQuestionaries: types.optional(types.array(types.string), [])
}).actions(self => ({
    sudoMe: () => {
        self.role === "ADMIN";
        return true;
    },
    deSudoMe: () => {
        if(self.id) {
            self.role === "LOCALUSER";
        } else {
            self.role === "ANONYMOUS";
        }
        return true;
    },
    addCompletedQuestionary: (questionary) => {
        self.completedQuestionaries.push(questionary.id);
    }
}))
.views(self => ({
    isAdmin: () => { console.log("\n User is admin?", self.role); return self.role === "ADMIN"; },
    isLoggedIn: () => ["ADMIN", "USER"].includes(self.role),
    hasCompletedQuestionary: (questionary) => { return self.completedQuestionaries.find(qs => qs === questionary.id) !== undefined}
}));
