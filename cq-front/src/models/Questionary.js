import { types, actions, views, destroy } from 'mobx-state-tree';
import uuid from 'uuid';

import { Question } from './Question';

/**
 * Questionary contains the data for a single questionary
 */
export const Questionary = types.model({
    id: types.identifier(),
    name: types.string,
    visibility: types.optional(types.enumeration(["PUBLIC", "INVITATION", "USER"]), "PUBLIC"),
    type: types.optional(types.enumeration(["GAMESHOW", "OPEN", "TIMED"]), "OPEN"),
    description: types.optional(types.string, ""),
    questions: types.optional(types.map(Question), {}),
    startQuestion: types.maybe(types.reference(Question)),
    state: types.enumeration(["DRAFT", "OPEN", "CLOSED", "ARCHIVED"])
}).actions(self => ({

    newQuestion: (data) => {
        const nq = Question.create({ ...data });
        self.questions.put(nq);
        return nq;
    },

    removeQuestion: (question) => {
        if (self.state === "OPEN") {
            return false;
        }

        return destroy(question);
    },

    setName: (name) => {
        self.name = name;
    },

    setDescription: (description) => {
        self.description = description;
    },

    setType: (type) => {
        self.type = type;
    },

    setVisibility: (visibility) => {
        self.visibility = visibility;
    },

    setState: (state) => {
        self.state = state;
    },

    update: (updateObject) => {
        const keys = Object.keys(updateObject);
        keys.forEach(key => {
            self[key] = updateObject[key];
        });
    },

    testAction: () => {
        console.log("questionary test action");
    },


    qMachineActionHandler: (action, params={}) => {
        if(action.do && Array.isArray(action.do)) {
            action.do.forEach(cmd => {
                if(self[cmd]) {
                    self[cmd](params);
                } else {
                    console.log("questionary has no command:", cmd);
                }

            });
        }
    }
})).views(self => ({
    listQuestionsAsArray: () => {
        return Array.from(self.questions.toJS().values());
    },
    getQuestionById: (id) => {
        return self.questions.get(id);
    },
    getQuestion: (id) => {
        return self.questions.get(id);
    },
    getFirstQuestion: () => {
        return self.startQuestion;
    },
    getNextQuestion: (answer) => {
        // const edge = self.edges.find(edge => edge.fromAnswer.id === answer.id);
        if (answer.nextQuestionId === "THE_END") {
            return null;
        }
        const nq = self.questions.get(answer.nextQuestionId);
        console.log("Get next question for answer", nq);
        return nq;
    },
    getSigmaGraphData: () => {


        const qarray = self.listQuestionsAsArray();
        const edgeList = qarray.reduce((list, question) => {
            const newEdges = question.answers.map(answer => {
                return {
                    id: answer.id,
                    source: question.id,
                    target: answer.nextQuestionId,
                    label: answer.text
                };
            });
            return [...list, ...newEdges];
        }, []);

        edgeList.push({
            id: "firstQuestion",
            source: "START",
            target: self.startQuestion.id
        });


        const startX = -100;
        const endX = 100;
        const questionCount = qarray.length;
        const step = (endX - startX) / questionCount;

        const sigmaGraph = {
            nodes: qarray.map((question, index) => {
                return {
                    id: question.id,
                    label: question.question,
                    size: 3,
                    x: startX + (index * step),
                    y: 0
                };
            }),
            edges: edgeList
        };

        sigmaGraph.nodes.push({
            id: "START",
            label: "START",
            size: 8,
            x: startX,
            y: 0
        });
        sigmaGraph.nodes.push({
            id: "THE_END",
            label: "END",
            size: 8,
            x: endX,
            y: 0
        });

        return sigmaGraph;
    },
    getGoChartFormat: () => {
        const qarray = self.listQuestionsAsArray();
        const edgeList = qarray.reduce((list, question) => {
            const newEdges = question.answers.map(answer => {
                return {
                    from: question.id,
                    to: answer.nextQuestionId,
                };
            });
            return [...list, ...newEdges];
        }, []);

        edgeList.push({
            from: "START",
            to: self.startQuestion.id
        });



        const goChart = {
            nodeDataArray: qarray.map((question, index) => {
                return {
                    key: question.id,
                    color: "blue"
                };
            }),
            linkDataArray: edgeList
        };

        goChart.nodeDataArray.push({
            key: "START",
            color: "green"
        });
        goChart.nodeDataArray.push({
            key: "THE_END",
            color: "red"
        });

        return goChart;
    }
}));

