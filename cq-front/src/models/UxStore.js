import { types, actions, views, getSnapshot, applySnapshot, destroy } from 'mobx-state-tree';
import { observable, action } from 'mobx';
import uuid from 'uuid';

// ["INIT","INDEX","QUESTIONARIES", "QUESTIONARY", "QUESTION", "STATS", "ADMIN", "RESULTS", "LOGIN", "LOGIN_PENDING", "STATISTICS"]),
const UxStore = types.model({
    mode: types.optional(types.enumeration(["FULL", "KIOSK", "MONITOR"]), "FULL"),
    state: types.optional(types.enumeration(["INIT", "INDEX", "QUESTIONARIES", "QUESTIONARY", "QUESTION", "STATISTICS", "MYRESULTS", "LOGIN", "ADMIN"]), "INIT"),
    loading: types.optional(types.boolean, true)
}).actions(self => ({
    setMode: (newMode) => {
        if(!self.loading) {
            self.mode = newMode;
        }
    },
    setState: (newState) => {
        if(!self.loading) {
            self.state = newState;
        }
        
    },
    bootingUp: () => {
        self.state === "INIT";
        self.loading = true;
    },
    bootingDone: (newState, newMode) => {
        self.state = newState;
        self.mode = newMode;
        self.loading = false;
    }
})).views(self => ({
    isLoading: () => {
        return self.loading;
    }
}))


export default UxStore;
