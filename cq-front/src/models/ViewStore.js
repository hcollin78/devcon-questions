import { observable, action, computed } from 'mobx';


class ViewStoreFactory {

    @observable state;
    @observable user;
    @observable questionary;
    @observable question;


    constructor() {
        this.state = "INDEX";
        this.user = null;
        this.questionary = null;
        this.question = null;
    }


    @action
    changeViewState(state) {
        this.state = state;
    }

    @action
    setQuestionary(questionary) {
        this.questionary = questionary;
    }

    @action
    setUser(user) {
        this.user = user;
    }

    @computed
    get isLoggedIn() {
        return this.user !== null;
    }


}

const ViewStore = new ViewStoreFactory();

export default ViewStore;