import { types, actions, views, destroy} from 'mobx-state-tree';
import uuid from 'uuid';

import { Answer } from './Answer';

export const Question = types.model({
    id: types.identifier(types.string),
    question: types.string,
    description: types.maybe(types.string),
    answers: types.array(Answer)
}).actions(self => ({

    newAnswer: (data) => {
        const ans = Answer.create({...data});
        self.answers.push(ans);
        return ans;
    },

    removeAnswer: (answer) => {
        destroy(answer);
    },

    update: (updateObject) => {
        const keys = Object.keys(updateObject);
        keys.forEach(key => {
            self[key] = updateObject[key];
        });
    },

    testAction: () => {
        console.log("question test action");
    },


    qMachineActionHandler: (action, params={}) => {
        if(action.do && Array.isArray(action.do)) {
            action.do.forEach(cmd => {
                if(self[cmd]) {
                    self[cmd](params);
                } else {
                    console.log("question has no command:", cmd);
                }

            });
        }
    }

})).views(self => ({
    isMyAnswer: (myAnswer=false) => {
        if(!myAnswer || !myAnswer.answer) {
            return false;
        }
        return myAnswer.answer.id = self.id;
    },
    getAnswer: (answerId) => {
        return self.answers.find(ans => ans.id === answerId);
    }
}));
