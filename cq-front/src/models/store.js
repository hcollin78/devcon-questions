import { types, actions, views, getSnapshot, applySnapshot, destroy, flow } from 'mobx-state-tree';
import uuid from 'uuid';

// APIs
import { saveMyAnswer } from '../api/AnswerApi';
import { createKioskUser } from '../api/UserApi';
import { userCompletesAQuestionary } from '../api/StoreApi';

// Models
import { Questionary } from './Questionary';
import { UX } from './Ux';
import { User } from './User';
import { MyAnswer } from './MyAnswer';

import notificationService from '../components/notifier/notificationService';

/**
 * Store contains the main store and single source of truth
 */
export const Store = types.model({
    questionaries: types.optional(types.map(Questionary), {}),
    myAnswers: types.optional(types.array(MyAnswer), []),
    users: types.optional(types.map(User), {}),
    ux: UX
})
    .actions(self => ({


        newQuestionary: (params) => {
            self.questionaries.put(Questionary.create(params));
            console.log(self);
        },

        removeQuestionary: (questionary) => {
            if(questionary.state === "OPEN") {
                return false;
            }

            return destroy(questionary);
        },

        addUser: (user) => {
            if(!self.users.get(user)) {
                self.users.put(user);
            }
        },
        
        addOrUpdateMyAnswer: (myAnswer) => {
            const answeredAlready = self.myAnswers.find(ans => ans.question.id === self.ux.question.id);
            if(answeredAlready) {
                console.log("Update answer to question", myAnswer);
                answeredAlready.updateMyAnswer(myAnswer);
            } else {
                console.log("Create new answer to question", myAnswer);
                
                self.myAnswers.push(MyAnswer.create({
                    user: self.ux.user,
                    question: self.ux.question,
                    questionary: self.ux.questionary,
                    answer: myAnswer
                }));
            }

            saveMyAnswer({user: self.ux.user, question: self.ux.question, questionary: self.ux.questionary, answer: myAnswer}).then(reply => {
                console.log("SAVED ANSWER!", reply);
            });

            
            return true;
        },

        setCurrentUser: (user) => {
            self.ux.setUser(user);
        },

        resetAnswers: () => {
            self.myAnswers.clear();
            return true;
        },

        saveToLocal: () => {
            window.localStorage.setItem("cinia_questionary_snapshot", JSON.stringify(getSnapshot(self)));
            return true;
        },

        resetLocal: () => {
            window.localStorage.clear();
            return true;
        },

        loadFromLocal: () => {
            const snapShot = window.localStorage.getItem("cinia_questionary_snapshot");
            // console.log("LOCAL SNAPSHOT: ", snapShot);
            if (snapShot) {
                try {
                    applySnapshot(self, JSON.parse(snapShot));
                } catch(err) {
                    console.error("ERROR: Could apply the snapshot from local storage\nResetting current localStorage snapshot.", snapShot);
                    self.resetLocal();
                }
            }
        },

        updateAllQuestionaries: (questionaries) => {
            const currentSnapShot = getSnapshot(self);
            const newState = Object.assign({}, currentSnapShot, questionaries);
            applySnapshot(self, newState);
        },

        /**
         * doAction function is an access point for the state machine actions.
         * @param {*} action 
         * @param {*} params 
         */
        doAction: (action, params, newState) => {
            switch (action) {

                case "setActiveQuestionary":
                    return self.ux.setQuestionary(params.questionary);

                case "setActiveQuestion":
                    return self.ux.setQuestion(params.question);

                case "setAnswerToQuestion":
                    return self.addOrUpdateMyAnswer(params.answer);

                case "moveToNextQuestion":
                    return self.ux.setQuestion(self.ux.questionary.getNextQuestion(params.answer));

                case "saveTargetState":
                    return self.ux.goToDefaultState(newState);

                case "resetCurrentQuestionary":
                    return self.resetAnswers();


                case "sudoUser": 
                    return self.ux.setAdminMode(true);
                case "deSudoUser": 
                    return self.ux.setAdminMode(false);

                case "loginAsUser":
                    return new Promise((resolve) => {
                        setTimeout(() => {
                            self.ux.login(params);
                            resolve(true);
                        }, 500);
                    });

                case "logoutUser":
                    self.ux.logout();
                    return true;

                case "loadingStart":
                    return self.ux.setLoadingTrue();

                case "loadingDone":
                    return self.ux.setLoadingFalse();

                case "saveStateToLocalStorage":
                    return self.saveToLocal();

                case "resetStateInLocalStorage":
                    return self.resetLocal();

                case "restoreStateFromLocalStorage":
                    return self.loadFromLocal();
                
                


                default:
                    return false;
            }
        },

        // qMachine Actions Below

        testAction: () => {
            console.log("store test action");
        },

        setFirstQuestionInQuestionaryAsActive: () => {
            const question = self.ux.questionary.getFirstQuestion();
            if(question) {
                self.ux.setQuestion(question);
            } else {
                console.warn("\nNo First question set for questionary", self.ux.questionary.name);
            }
            
        },

        selectQuestionary: (params) => {
            if(params.questionary) {
                self.ux.setQuestionary(params.questionary);
            }
        },

        selectNextQuestion: (answer) => {
            self.ux.setQuestion(self.ux.questionary.getNextQuestion(answer));
        },

        clearSelectedQuestion: () => {
            self.ux.setQuestion(null);
        },

        clearSelectedQuestionary: () => {
            self.ux.setQuestionary(null);
        },

        clearPreviousAnswers: () => {
            self.myAnswers.clear();
        },

        createNewKioskUser: flow(function* () {
            const user = yield createKioskUser();
            console.log("NEW KIOSK USER", user);
            self.users.put(user);
            self.ux.setUser(user);
        }),

        completeQuestionary: flow(function* (params) {
            self.ux.user.addCompletedQuestionary(self.ux.questionary);
            const res = yield userCompletesAQuestionary({
                userId: self.ux.user.id,
                questionaryId: self.ux.questionary.id
            });
        }),

        userHasCompletedQuestionary: (params) => {
            return self.ux.user.hasCompletedQuestionary(params.questionary);
        },

        /**
         * Main machine handler for QuestionaryMachine actions
         */
        qMachineActionHandler: (action, params={}) => {
            if(action.do && Array.isArray(action.do)) {
                action.do.forEach(cmd => {
                    if(self[cmd]) {
                        self[cmd](params);
                    } else {
                        console.log("Store has no command:", cmd);
                    }
                });
            }

            // Check if questionary wants to do something
            if(self.ux.questionary) {
                self.ux.questionary.qMachineActionHandler(action, params);
            }

            // Check if question wants to do something
            if(self.ux.question) {
                self.ux.question.qMachineActionHandler(action, params);
            }

            // Do we need to change the viewState? Happens only in store
            if(action.viewState) {
                if(Array.isArray(action.viewState)) {
                    for(let i=0; i < action.viewState.length ; i++) {
                        const viewCond = action.viewState[i];
                        if(viewCond.condition && self[viewCond.condition]) {
                            if(self[viewCond.condition](params)) {
                                self.ux.setViewState(viewCond.viewState);
                                i = action.viewState.length + 1;
                            }
                        }
                        if(viewCond.condition === undefined) {
                            self.ux.setViewState(viewCond.viewState);
                            i = action.viewState.length + 1;
                        }
                    }
                } else {
                    self.ux.setViewState(action.viewState);
                    console.log("Change view state",action.viewState);
                }
            }

            // Any post actions (ONLY IN STORE!)
            if(action.post && Array.isArray(action.post)) {
                action.post.forEach(cmd => {
                    if(self[cmd]) {
                        self[cmd](params);
                    } else {
                        console.log("Store has no post command:", cmd);
                    }
                });
            }
        }
    }))
    .views(self => ({

        getUserById: (userId) => {
            return self.users.get(userId);
        },

        getQuestionary: (qid) => {
            return self.questionaries.get(qid);
        },

        listQuestionariesAsArray: () => {
            return Array.from(self.questionaries.toJS().values());
        },

        getMyAnswers: (questionary=false) => {
            if(!questionary) {
                return self.myAnswers;
            }


            return self.myAnswers.filter(mans => mans.questionary.id === questionary.id);
        },

        /**
         * Return currently stored answer to currently to a question of currently active questionary
         */
        getMyAnswerTo: (question = false) => {
            if (!question) {
                question = self.ux.question;
            }

            const myAnswer = self.myAnswers.find(myAns => myAns.question.id === question.id);
            return myAnswer ? myAnswer : null;
        },

        currentUserIsAdmin: () => {
            return self.ux.user && self.ux.user.isLoggedIn() && self.ux.user.isAdmin();
        }
    }));

    // This is the default baseState that is used to populate the first draft of the store before
    // loading the current real state from the server.
    export const defaultBaseState = {
        ux: {
            "viewState": "INIT",
            "user": "anonymous",
            "adminMode": false
        },
        "myAnswers": [],
        "users": {
            "admin" : {
                "id": "admin",
                "email": "henrik.collin@cinia.fi",
                "name": "Administrator",
                "role": "ADMIN"
            },
            "anonymous" : {
                "id": "anonymous",
                "email": "",
                "name": "",
                "role": "ANONYMOUS"
    
            }
        },
        "questionaries": {}
    };