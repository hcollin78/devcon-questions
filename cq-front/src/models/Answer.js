import { types, actions, views} from 'mobx-state-tree';

export const Answer = types.model({
    id: types.identifier(types.string),
    text: types.string,
    value: types.string,
    nextQuestionId: types.maybe(types.string)
})
.actions(self => ({
    setAsMyAnswer: () => {
        console.log("TODO!");
    },
    update: (updateObject) => {
        const keys = Object.keys(updateObject);
        keys.forEach(key => {
            self[key] = updateObject[key];
        });
    }
}))
.views(self => ({
    lastQuestion: () => {
        return self.nextQuestionId === null;
    },
    isLastQuestion: () => {
        return self.nextQuestionId === "THE_END";
    }
}));

