import { types, actions, views} from 'mobx-state-tree';

import { Questionary } from './Questionary';
import { Question } from './Question';
import { Answer } from './Answer';
import { User } from './User';

/**
 * MyAnswer is a model for an answer to a question in questionary
 */
export const MyAnswer = types.model({
    questionary: types.reference(Questionary),
    question: types.reference(Question),
    user: types.maybe(types.reference(User)),
    answer: types.reference(Answer)
})
.actions(self => ({
    updateMyAnswer: (newAnswer) => {
        self.answer = newAnswer;
    }
}));

