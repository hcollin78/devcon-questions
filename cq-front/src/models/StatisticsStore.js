import { observable, action, computed } from 'mobx';
import {getStatisticalAnswers, listenForStatisticalUpdates} from '../api/AnswerApi';

class StatisticsStore {

    @observable questionary = null;
    @observable answers = [];
    @observable loading = true;

    @action
    startStore(questionary) {
        this.questionary = questionary;

        getStatisticalAnswers(this.questionary).then((reply) => {
            this.setAnswers(reply.data.answers);
            this.loading =false;
    
            this.listener = listenForStatisticalUpdates(this.questionary, action((reply) => { 
                    this.newAnswer(reply.data);
            }));
        }).catch(err => {
            this.loading =false;
            console.error("ERROR", err);
        });
    }

    @action
    stopUpdates() {
        if(this.listener) {
            this.listener();
        }
    }

    @action
    setAnswers = (answers) => {
        this.answers = answers;
    }

    @action
    newAnswer(answer) {
        //TODO: Check if actually update to existing answer...
        
        const existingId = this.answers.findIndex(ans => {return ans.question === answer.question && ans.user === answer.user});
        console.log("new answer: ", answer, existingId);
        if(existingId === -1) {
            this.answers.push(answer);    
        } else {
            //TODO UPDATE HERE!
        }
        
    }


    getAnswersForQuestion(questionId) {
        this.answers.filter(ans => ans.question === questionId);
    }

    getQuestions() {
        return this.questionary.listQuestionsAsArray();
    }

    getAnswerChoisesForQuestion(qid) {
        return this.questionary.getQuestion(qid).answers;
    }

    getUserAnswersForAnswerChoise(answerId) {
        return this.answers.filter(ans => ans.answer === answerId);
    }

}

const statisticsStore = new StatisticsStore();
export default statisticsStore;