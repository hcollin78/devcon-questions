import { types, actions, views, getRoot} from 'mobx-state-tree';
// import uuid from 'uuid';

import { Questionary } from './Questionary';
import { Question } from './Question';
// import { Answer } from './Answer';
import { User } from './User';

/**
 * UX Model handles the visible state of the application
 */
export const UX = types.model({
    viewState: types.enumeration(["INIT","INDEX","QUESTIONARIES", "QUESTIONARY", "QUESTION", "STATS", "ADMIN", "MYRESULTS", "LOGIN", "LOGIN_PENDING", "STATISTICS"]),
    viewMode: types.optional(types.enumeration(["FULL", "KIOSK", "MONITOR"]), "FULL"),
    questionary: types.maybe(types.reference(Questionary)),
    question: types.maybe(types.reference(Question)),
    user: types.maybe(types.reference(User)),
    adminMode: types.optional(types.boolean, false),
    loading: types.optional(types.boolean, false)
}).actions(self => ({

    setQuestionary: (questionary) => {
        self.questionary = questionary;
        return true;
    },

    setQuestion: (question) => {
        self.question = question;
        return true;
    },

    setAdminMode: (toValue)=> {
        self.adminMode = toValue;
        return true;
    },

    setUser: (user) => {
        self.user = user;
        getRoot(self).addUser(user);
        return true;
    },

    setViewState: (state) => {
        self.viewState = state;
        return true;
    },
    logout: () => {
        self.user = getRoot(self).getUserById("anonymous");
    },
    login: (user) => {
        self.user = user;
    },
    setLoadingTrue: () => {
        self.loading = true;
        return true;
    },
    setLoadingFalse: () => {
        self.loading = false;
        return true;
    },

    setMode: (toMode) => {
        self.viewMode = toMode;
        return true;
    },

    goToDefaultState: (newState) => {
        self.setViewState(newState.value);
        return true;
    }
}))
.views(self => ({
    
    isLoggedIn: () => {
        return (self.user !== null ? self.user.isLoggedIn() : false);
    },
    inAdminMode: () => {
        return self.adminMode;
    },
    viewModeClass: () => {
        const vm = { "FULL": "full-site-mode", "KIOSK": "kiosk-mode", "MONITOR": "stats-mode"};
        return vm[self.viewMode];
    },
    defaultView: () => {
        const vm = {
            "FULL": "INDEX",
            "KIOSK": "QUESTIONARIES",
            "MONITOR": "QUESTIONARIES"
        };
        return vm[self.viewMode];
    }
}));
