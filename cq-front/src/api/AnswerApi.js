import connection from './Connection';

export function loadMyAnswers(userId) {

}

export function saveMyAnswer(answer) {
    return new Promise((resolve, reject) => {
        connection.sendMessage("addAnswer", {answer: answer}, (reply) => {
            console.log("Answer added!", reply);
            resolve();
        }); 
    });
}

export function resetMyAnswersToQuestionary(user, questionary) {

}

/**
 * This returns a list of answers given for target questionary. It only provides ids to objects.
 * @param {} questionary 
 */
export function getStatisticalAnswers(questionary) {
    return new Promise((resolve, reject) => {
        connection.sendMessage("getAnswersToQuestionary", {questionaryId: questionary.id}, (reply) => {
            if(reply.data.error !== undefined) {
                reject(reply.data);
            }
            resolve(reply);

        })
    })
}

export function listenForStatisticalUpdates(questionary, callback) {
    return connection.addListener("broadcastNewAnswer", (broadcast) => {
        if(broadcast.data.data.questionary === questionary.id) {
            callback(broadcast.data);
        }
    });
}
