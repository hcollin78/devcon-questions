import connection from './Connection';

/**
 * This loads the current and latest state from the server and localstorage and passes it back.
 */
export function getInitialState() {
    return new Promise((resolve, reject) => {
        console.log("Ask for initial State");
        connection.sendMessage("getCurrentState", {}, (reply) => {{
            console.log("Retrieved initial data set from server", reply);
            resolve(reply.data);
        }});
    });
    
}

export function getLocalState() {
    const localSnapShot = window.localStorage.getItem("cinia_questionary_snapshot");
    if(localSnapShot) {
        return JSON.parse(localSnapShot);
    }
    return false;
}


export function clearLocalState() {
    window.localStorage.clear();    
    return true;
}

export function listenToChangesInQuestionaries(callback) {
    return connection.addListener("broadcastQuestionaryUpdate", (broadcast) => {
        callback(broadcast.data);
    });
}

// Common function used to send simple socket commands to server with reply
function simpleSocketSendWithReply(action, params) {
    return new Promise((resolve, reject) => {
        connection.sendMessage(action, params, (reply) => {
            if(reply.data.error !== undefined) {
                reject(reply.data);
            }
            resolve(reply);
        })
    });
}



export function reloadQuestionaries() {
    return simpleSocketSendWithReply("reloadQuestionaries");
}

export function userCompletesAQuestionary(params) {
    return  simpleSocketSendWithReply("completeQuestionary", params);
}

export function getFullSnapShot() {
    return simpleSocketSendWithReply("getFullSnapShot");
}

export function setFullSnapShot(params) {
    return simpleSocketSendWithReply("setFullSnapShot", params);
}

// QUESTIONARIES

export function createQuestionary(params) {
    return  simpleSocketSendWithReply("createQuestionary", params);
}

export function updateQuestionary(params) {
    return  simpleSocketSendWithReply("updateQuestionary", params);
}

export function removeQuestionary(params) {
    return  simpleSocketSendWithReply("removeQuestionary", params);
}


// QUESTIONS FOR QUESTIONARY

export function createQuestion(params) {
    return  simpleSocketSendWithReply("createQuestion", params);
}

export function updateQuestion(params) {
    return  simpleSocketSendWithReply("updateQuestion", params);
}

export function removeQuestion(params) {
    return  simpleSocketSendWithReply("removeQuestion", params);
}


// ANSWER OPTIONS FOR QUESTION

export function createAnswer(params) {
    return  simpleSocketSendWithReply("createAnswer", params);
}

export function updateAnswer(params) {
    return  simpleSocketSendWithReply("updateAnswer", params);
}

export function removeAnswer(params) {
    return  simpleSocketSendWithReply("removeAnswer", params);
}