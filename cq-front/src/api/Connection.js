import uuid from 'uuid';
import CONFIG from '../config';

export const defaultConfiguration = {
    protocol: "ws",
    domain: document.location.hostname,
    port: CONFIG.webSocketPort,
    path: "",
    pingDelay: CONFIG.pingDelay,
    logLevels: CONFIG.isDevelopement ? ["DEBUG", "INFO", "WARN","ERROR"] : ["WARN","ERROR"]
};

export const liveConfiguration = {
    protocol: "ws",
    domain: "35.204.179.209",
    port: "8080",
    path: "",
    pingDelay: 10000,
    logLevels: ["DEBUG","INFO","WARN","ERROR"]
}

class Connection {

    connection = null;
    config = null;
    messageListeners = [];
    sendQueue = [];
    
    connectionTries = 0;

    connectionId = null;

    constructor(config=false) {
        this.config = config ? config : defaultConfiguration;
        this.pinger = null;
        // this.addListener("connectionEstablished", this.setConnectionId, true);
        // this.openConnection();
    }

    logger(type, ...msg) {
        if(!this.config.logLevels.includes(type)) {
            return;
        }
        switch(type) {
            case "DEBUG":
                console.debug(msg);
                break;
            case "INFO":
                console.log(msg);
                break;
            case "WARN":
                console.warn(msg);
                break;
            case "ERROR":
                console.error(msg);
                break;
            default:
                break;
        }
    
    }

    openConnection(config=false) {
        this.config = config ? config : (this.config ? this.config : defaultConfiguration);
        console.log(this.config);
        this.addListener("connectionEstablished", this.setConnectionId, true);
        const url = `${this.config.protocol}://${this.config.domain}:${this.config.port}${this.config.path}`;
        this.logger("INFO","CONNECTION: Server url", url);
        this.connectionTries++;
        this.stopPing();
        
        try {
            this.connection = new WebSocket(url);
            this.connection.addEventListener('error', this.eventHandlerError);
            this.connection.addEventListener('open', this.eventHandlerConnectionOpened);
            this.connection.addEventListener('message', this.eventHandlerMessage);
            return true;
            
        } catch(err) {
            this.logger("ERROR","CONNECTION:ERROR: Could not establish connection to server", err);
            return false;
        }
    }

    reopenConnection() {
        const success = this.openConnection(this.config);
        if(!success) {
            setTimeout(() => {
                this.reopenConnection();
            }, 1000);
        }
    }

    setConnectionId = (msg) => {
        this.connectionId = msg.data.connectionId;
        this.logger("INFO","CONNECTION: Connection Established", this.connectionId);
        this.connectionTries = 0;
        this._runQueue();
    };

    eventHandlerError = (err)  => {
        // this.logger("ERROR",("ERROR occured", err, this.connection);
        if(this.connection === null || this.connection === undefined || this.connection.readyState === WebSocket.CLOSED) {
            
            if(this.connectionTries >=5) {
                this.logger("ERROR","CONNECTION:ERROR: CANNOT CONNECT TO THE SERVER! ALL FAILS! DIE!");
                return;
            }
            
            const waitTime = 500 * (this.connectionTries*this.connectionTries);
            this.logger("WARN","CONNECTION: WARN: Try connecting again in " + (waitTime / 1000) + " seconds.");
            setTimeout(() => {
                this.openConnection();
            }, waitTime);
            

        }
    }

    eventHandlerConnectionOpened = (e) => {
        this.logger("INFO","CONNECTION: Connection Open");
        this.startPing();
    };

    eventHandlerMessage = (e) => {
        // this.logger("INFO","Message Recieved", e);
        const msg = JSON.parse(e.data);
        // this.logger("INFO","MESSAGE:", msg);
        this.logger("INFO","CONNECTION: Incoming...", msg.key);
        this.messageListeners.forEach(listenerObject  => {
            this.logger("INFO","\t Check: ", listenerObject.key, msg.key);
            if(listenerObject.key === msg.key) {
                this.logger("INFO","CONNECTION: Run listener", listenerObject.key);
                listenerObject.func(msg);
                if(listenerObject.runOnce) {
                    this.logger("INFO","CONNECTION: Remove listener", listenerObject.key, this.messageListeners.length);
                    listenerObject.removeMe();
                }
            }
        });
    };

    eventHandlerConnectionClosed = (e) => {
        this.logger("INFO","CONNECTION: Connection Closed", e);
        this.stopPing();
        
    };

    /**
     * Send message to server
     * @param {*} key The key of the command to be sent
     * @param {*} data Data passed to the server
     * @param {*} replyCallback OPTIONAL: Expect a reply and run this callback the reply comes
     */
    sendMessage(key, data, replyCallback=false) {
        
        const replyKey = key+"_reply_"+uuid.v4();
        
        const msgObj = {
            key: key,
            data: data,
            reply: replyCallback !== false ? replyKey : false
        };

        if(replyCallback) {
            this.addListener(replyKey, replyCallback, true);
        }

        this.sendQueue.push(msgObj);

        this.logger("INFO","CONNECTION: Send message ", msgObj);
        
        if(this.connection.readyState === WebSocket.OPEN) {
            this._runQueue();
        }
    }


    _runQueue() {
        if(this.connection.readyState === WebSocket.CLOSED || this.connection.readyState === WebSocket.CLOSING) {
            this.openConnection();
            setTimeout(() => {
                this._runQueue();
            }, 500);
        }
        let waitTime = 0;
        if(this.connection.readyState === WebSocket.CONNECTING) {
            waitTime = 250;
        }        
        setTimeout(() => {
            try {
                this.logger("INFO","CONNECTION: Execute send message queue");
                while(this.sendQueue.length > 0) {
                    const msg = this.sendQueue.shift();
                    this.connection.send(JSON.stringify(msg));
                }       
                
                
            } catch (err) {
                this.logger("ERROR","CONNECTION: ERROR: FAILED TO SEND MESSAGES", err);
            }
        }, waitTime);
    }

    addListener(msgKey, callback, runOnlyOnce=false) {
        const id = uuid.v4();
        
        const remover  = () => {
            const ind = this.messageListeners.findIndex(lo => lo.listenerId === id);
            if(ind > -1) {
                this.messageListeners.splice(ind, 1);
                return true;
            }
            return false;
        
        };
        const listenerObject = {
            listenerId: id,
            key: msgKey,
            func: callback,
            runOnce: runOnlyOnce,
            removeMe: remover
        };
        this.messageListeners.push(listenerObject);
        this.logger("INFO","CONNECTION: New listener ", msgKey, " Run only once? ", runOnlyOnce);
        return remover;
        
    }

    ping() {
        if(this.connection.readyState === WebSocket.CLOSED || this.connection.readyState === WebSocket.CLOSING) {
            this.logger("INFO","ERRO: Connection closed! Reopening...");
            this.openConnection();            
        } else {
            if(this.connection.readyState === WebSocket.OPEN) {
                this.sendMessage("ping", {}, (reply) => {
                    if(reply.data === "pong") {
                        this.logger("INFO","CONNECTION: Ping");                    
                    }
                });
            }
        }
    }


    startPing() {
        this.stopPing();
        this.pinger = setInterval(() => {
            this.ping();
        }, this.config.pingDelay);
    }

    stopPing() {
        if(this.pinger) {
            clearInterval(this.pinger);
            this.pinger = null;
        }
    }

}

const connection = new Connection();
export default connection;