import connection from './Connection';

import { User } from '../models/User';
import { sha256 } from 'js-sha256';



/**
 * Create a local user in the backend and return the MST model in Promise
 */
export function createLocalUser() {
    return new Promise((resolve) => {
        connection.sendMessage("createLocalUser", {}, (reply) => {
            console.log("UserApi: New localUser Created", reply);
            const user = User.create(reply.data);
            resolve(user);
        });
    })
}

export function createKioskUser() {
    return new Promise((resolve) => {
        connection.sendMessage("createKioskUser", {}, (reply) => {
            console.log("UserApi: New Kiosk user Created", reply);
            const user = User.create(reply.data);
            resolve(user);
        });
    });
}

/**
 * Load user information from the backend by id
 * @param {*} userId 
 */
export function loadUser(userId) {
    return new Promise((resolve, reject) => {
        connection.sendMessage("getUserById", {userId: userId}, (reply) => {
            console.log("reply", reply)
            if(reply.data && reply.data.error !== undefined) {
                reject(reply.data);
            }
            if(reply.data) {
                const user = User.create(reply.data);
                resolve(user);
            } else {
                console.error("Loaded user is undefined", reply.data);
                reject(reply);
            }
            
        });
    });
}

export function identifyUser(viewMode="FULL") {
    return new Promise((resolve, reject) => {

    
        // Check if user information is stored to localhost
        const userInLocalStorage = window.localStorage.getItem("currentUserInformation");

        console.log("\n\t User in local?", userInLocalStorage);
        // User information already in the localStorage, use it if possible. Login with that user too.
        // If that information does not match any user in server, a new user is created automatically.
        if(userInLocalStorage) {
            const localUser  = JSON.parse(userInLocalStorage);
            loadUser(localUser.id).then(user => {
                resolve(user);
                return;
            }).catch(err => {
                console.log("Could not load the user in localstorage from server. Creating a new one!");
                createLocalUser().then(user => {    
                    resolve(user);
                });
            });
        } else {
            console.log("\tCreate localUser!");
            // No user information in localStorage or no user found from the serevr, create an anonymous localUser
            // This can be converted later on to a normal user by registering
            createLocalUser().then(user => {    
                resolve(user);
            });
        }
         
        
    
    });
}


export function storeUserToLocalStorage(user) {
    if(user.role !== "KIOSK") {
        window.localStorage.setItem("currentUserInformation", JSON.stringify(user));
    }
}


/**
 * Turn on the admin mode by checking it in the backend first
 * @param {*} password 
 */
export function confirmSudoAdminPassword(password) {
    return new Promise((resolve, reject) => {
        const passwd = sha256(connection.connectionId + "-" + password);
        connection.sendMessage("sudoAsAdmin", { pwd: passwd }, (reply) => {
            if(reply.data === "OK") {
                resolve(true);
            } else {
                reject("Invalid Login");
            }
        })
    });
}

export function turnOnAdminMode(store) {
    store.ux.setAdminMode(true);
}

export function turnOffAdminMode(store) {
    store.ux.setAdminMode(false);
}
