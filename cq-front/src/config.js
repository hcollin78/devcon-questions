

const ORIGINALCONFIGS = {
    // Environment
    isDevelopment: process.env.NODE_ENV === "development",
    isProduction: process.env.NODE_ENV !== "development",

    // Web Socket connection
    webSocketPort: 8080,
    pingDelay: 20000,

    // UI Helpers
    showInfoBox: process.env.NODE_ENV === "development",
    connectionIndicatorAlwaysOn: process.env.NODE_ENV === "development",

    // Notification 
    notificationLiveTime: 4000
}

const CONFIG = {...ORIGINALCONFIGS};

export function overRideConfig(key, value) {
    CONFIG[key] = value;
}

export function resetConfigs(key=null) {
    if(key!==null && CONFIG[key] !== undefined) {
        if(ORIGINALCONFIGS[key]) {
            CONFIG[key] = ORIGINALCONFIGS[key]
        } else {
            delete(CONFIG[key])
        }
    } else {
        Object.keys(CONFIG).forEach(lk => {
            if(ORIGINALCONFIGS[lk]) {
                CONFIG[lk] = ORIGINALCONFIGS[lk];
            } else {
                delete(CONFIG[lk]);
            }
        })
    }

}
export default CONFIG;