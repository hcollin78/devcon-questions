import React from 'react';
import { observer, inject } from 'mobx-react';

import './main-menu.scss';

@inject("store", "viewMachine")
@observer
export default class MainMenu extends React.Component {

    // login = () => {
    //     const admin = this.props.store.getUserById("admin");
    //     console.log("LOGIN!", admin);
    //     if (admin) {
    //         this.props.store.ux.setUser(admin);
    //     }
    // };

    // logout = () => {
    //     this.props.store.ux.logout();

    // };

    handleMenuClick = (action) => {
        console.log(action);
        this.props.viewMachine.action(action);

    }

    render() {

        if(this.props.store.ux.viewState === "LOADINGSCREEN") {
            return null;
        }

        // console.log("\n\nMAINMENU RENDER: ", this.props.store.ux);
        // if(this.props.store.ux.user)
        const isAdmin = this.props.store.ux.inAdminMode();
        const loggedIn = isAdmin; //this.props.store.ux.isLoggedIn();
        const questionary = this.props.store.ux.questionary ? this.props.store.ux.questionary.name : false;

        // console.log("\n\nMAINMENU RENDER: ", this.props.store.ux.user.role, loggedIn, isAdmin);

        const loading = this.props.store.ux.loading;
        // if (this.props.store.ux.viewMode !== "FULL") {
        const menuClasses = ["admin-only-menu", isAdmin ? " logged-in": ""].join(" ");
        return (
            <nav className={menuClasses}>
                {isAdmin && (
                    <React.Fragment>
                        {/* <MenuItem text="ADMIN" value="goToAdmin" onClick={this.handleMenuClick} /> */}
                        <MenuItem text="LOGOUT" value="goToLogout" onClick={this.handleMenuClick} />
                    </React.Fragment>
                )}
                {!loggedIn && (
                    <MenuItem text="Login" value="goToLogin" onClick={this.handleMenuClick} />
                )}
            </nav>);
        // }

        // return (
        //     <nav className="main-menu">

        //         <div className="menu-logo">
        //             CQ?
        //         </div>


        //         {/* <MenuItem text="Frontpage" value="goToIndex" onClick={this.handleMenuClick} /> */}
        //         {isAdmin && (
        //             <MenuItem text="Administration" value="goToAdmin" onClick={this.handleMenuClick} />
        //         )}
        //         <MenuItem text="Questionaries" value="goToQuestionaries" onClick={this.handleMenuClick} />
        //         {questionary && this.props.store.ux.questionary.state !== "DRAFT" && (
        //             <MenuItem text={questionary} value="goToQuestionary" onClick={this.handleMenuClick} />
        //         )}

        //         {loggedIn && (
        //             <MenuItem text="Logout" value="goToLogout" onClick={this.handleMenuClick} />
        //         )}

        //         {!loggedIn && (
        //             <MenuItem text="Login" value="goToLogin" onClick={this.handleMenuClick} />
        //         )}



        //     </nav>
        // )
    }
}


@inject("viewMachine")
@observer
class MenuItem extends React.Component {

    handleMenuClick = () => {
        this.props.onClick(this.props.value);
    }

    render() {

        if (!this.props.viewMachine.hasAction(this.props.value)) {
            console.warn("Unknown action", this.props.value);
            return null;
        }

        return (
            <div className="menu-item" onClick={this.handleMenuClick}>
                {this.props.text}
            </div>
        );
    }

}