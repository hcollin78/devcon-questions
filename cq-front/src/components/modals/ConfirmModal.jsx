import React from 'react';

import './modals.scss';

export default class ConfirmModal extends React.Component {


    confirm = (e) => {
        this.props.confirm();
    }

    cancel = (e) => {
        this.props.cancel();
    }

    

    render() {

        if(!this.props.open) {
            return null;
        }
        return (
            <div className="modal-container">
                <div className="modal">
                    <header>
                    <h1>Are you sure?</h1>
                    </header>

                    <div className="modal-content">
                    
                    
                    </div>


                    <footer>
                    <button className="cancel" onClick={this.cancel}>Cancel</button>
                    <button className="confirm" onClick={this.confirm}>Confirm</button>
                    </footer>

                </div>
            </div>
        );
    }
}