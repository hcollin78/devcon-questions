import React from 'react';
import { observer } from 'mobx-react';
import { observable, action } from 'mobx';

import './label-input.scss';

@observer
export default class LabelInput extends React.Component {

    @observable value = "";

    constructor(props) {
        super(props);

        this.value = props.value;
    }

    onChange = (e)  => {
        this.value = e.currentTarget.value;
        this.props.onChange(this.props.name, this.value);
    }

    render() {

        
        return (
            <div className="label-input">
                <label>{this.props.text}</label>
                <input type="text" value={this.value} onChange={this.onChange} name={this.props.name} />
            </div>
        )

    }

}