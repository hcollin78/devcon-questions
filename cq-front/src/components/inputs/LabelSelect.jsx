import React from 'react';
import { observer } from 'mobx-react';
import { observable, action } from 'mobx';

import './label-input.scss';

@observer
export default class LabelSelect extends React.Component {

    @observable value = "";

    constructor(props) {
        super(props);

        this.value = props.value;
    }

    onChange = (e)  => {
        this.value = e.currentTarget.value;
        this.props.onChange(this.props.name, this.value);
    }

    render() {

        
        return (
            <div className="label-input">
                <label>{this.props.text}</label>
                <select value={this.value} onChange={this.onChange} name={this.props.name}>
                    {this.props.choises.map((choise,ind) => (
                        <option value={choise.value} key={ind}>{choise.text}</option>
                    ))}
                </select>
            </div>
        )

    }

}