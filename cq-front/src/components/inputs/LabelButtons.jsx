import React from 'react';
import { observer } from 'mobx-react';
import { observable, action } from 'mobx';

import './label-input.scss';

@observer
export default class LabelButtons extends React.Component {

    @observable value = "";

    constructor(props) {
        super(props);

        this.value = props.value;
    }

    onChange = (e)  => {
        this.value = e.currentTarget.value;
        this.props.onChange(this.props.name, this.value);
    }

    render() {
        
        return (
            <div className="label-input">
                <label>{this.props.text}</label>
                <div className="buttons">
                    {this.props.choises.map((choise,ind) => (
                        <button key={ind} 
                                onClick={this.onChange} 
                                className={this.props.value === choise.value ? "selected" :""}
                                value={choise.value}>{choise.text}</button>
                    ))}
                </div>
            </div>
        )

    }

}