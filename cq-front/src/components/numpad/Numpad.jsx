import React from 'react';
import { observer, inject } from 'mobx-react';
import { observable, action } from 'mobx';

import './numpad.scss';

@observer
export default class Numpad extends React.Component {

    @observable value = "";
    @observable open = false;
    
    constructor(props) {
        super(props);
    }



    @action
    clear = () => {
        this.value = "";
    }

    @action
    addNumber = (e) => {
        
        if(this.value.length >=8 ) {
            return;
        }

        const num = e.currentTarget.value;
        this.value = this.value + num;
    }


    @action
    confirm = (e) => {
        this.props.onConfirm(this.value);
        this.clear();
    }

    @action
    close = () => {
        this.props.onCancel();
        this.clear();
    }
    
    render() {

        const screenData = {
            text: this.props.helpText ? this.props.helpText : "pin",
            classes: ["screen", "help"]
        }

        if(this.props.errorText && this.props.errorText.length > 0 && this.value.length === 0) {
            screenData.text = this.props.errorText;
            screenData.classes = ["screen", "error"];
        }

        if(this.value.length > 0) {
            screenData.text = Array(this.value.length+1).join("*") ;
            screenData.classes = ["screen"];
        }

    
        return (
            <div className="numpad-container">
                <div className="numpad">

                        <button onClick={this.close} className="close">X</button>

                    <div className={screenData.classes.join(" ")}>
                       {screenData.text}
                    </div>

                    <div className="numbers">
                        <button value="1" onClick={this.addNumber} disabled={this.props.disabled}>1</button>
                        <button value="2" onClick={this.addNumber} disabled={this.props.disabled}>2</button>
                        <button value="3" onClick={this.addNumber} disabled={this.props.disabled}>3</button>
                        <button value="4" onClick={this.addNumber} disabled={this.props.disabled}>4</button>
                        <button value="5" onClick={this.addNumber} disabled={this.props.disabled}>5</button>
                        <button value="6" onClick={this.addNumber} disabled={this.props.disabled}>6</button>
                        <button value="7" onClick={this.addNumber} disabled={this.props.disabled}>7</button>
                        <button value="8" onClick={this.addNumber} disabled={this.props.disabled}>8</button>
                        <button value="9" onClick={this.addNumber} disabled={this.props.disabled}>9</button>
                        <button onClick={this.clear} disabled={this.props.disabled} className="clear">clear</button>
                        <button value="0" onClick={this.addNumber} disabled={this.props.disabled}>0</button>
                        <button onClick={this.confirm} disabled={this.props.disabled} className="go">GO</button>
                    </div>
                </div>  
            </div>
            
        )
    }
}