import React from 'react';
import {observable, action } from 'mobx';
import {observer} from 'mobx-react';

import connection from '../../api/Connection';

import notificationService from '../notifier/notificationService';

import './indicator.scss';

import CONFIG from '../../config';


@observer
export default class ConnectionIndicator extends React.Component {

    @observable currentStatus = 0
    
    constructor(props) {
        super(props);
        this.currentStatus = connection.connection.readyState;

        this.lastReopen = Date.now();

        
        this.startTesting();
    }

    startTesting(waitTime=500) {
        if(this.tester) {
            clearInterval(this.tester);
        }
        this.tester = setInterval(this.checkStatus, waitTime);
    }

    @action
    checkStatus = () => {
        const oldStatus = this.currentStatus;
        this.currentStatus = connection.connection.readyState;

        // Restart normal test cycle
        if(oldStatus > this.currentStatus && this.currentStatus !== 0) {   
            this.startTesting(500);
            notificationService.success("Connection to the server re-established. Have a nice day!");
        }

        // Reopen connection and slow the test cycle
        if(this.currentStatus > 1 && oldStatus === 1) {
            notificationService.warn("Connection to server lost. Trying to reconnect");
            this.reopen();
        }

        if(this.currentStatus > 1 && oldStatus > 1) {
            const nowIs = Date.now();
            const deadSince = nowIs - this.lastReopen;

            if(deadSince > 10000) {
                this.reopen();
            }
        }
    }

    reopen() {
        connection.reopenConnection();
        this.startTesting(1000);
        this.lastReopen = Date.now();
    }

    render() {
        const statusText = ["CONNECTING", "OPEN", "CLOSING", "CLOSED"];
        const classes = ["connection-indicator", statusText[this.currentStatus]].join(" ");
        if(CONFIG.connectionIndicatorAlwaysOn && this.currentStatus === 1) {
            return null;
        }
        return (
            <div className={classes} />    
        )
    }
}