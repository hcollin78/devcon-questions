import React from 'react';

import './graph.scss';

export default class ChartTest extends React.Component {


    render() {

    
        const graph = this.props.questionary.getSigmaGraphData();
        
        const chart = [
            {
                question: "Dev?",
                answers: [
                    {
                        answer: "Yes",
                        subQuestion: {
                            question: "Front?",
                            answers: [
                                {
                                    answer: "Yes",
                                    subQuestion: {
                                        question: "Favored JS Framework?"   ,
                                        answers: [
                                            {
                                                answer: "React"
                                            },
                                            {
                                                answer: "Angular"
                                            },
                                            {
                                                answer: "Vue"
                                            }
                                        ]
                                    }
                                },
                                {
                                    answer: "No",
                                    subQuestion: {
                                        question: "Backend?",
                                        answers: [
                                            {
                                                answer: "Yes",
                                                subQuestion: {
                                                    question: "Language?"   ,
                                                    answers: [
                                                        {
                                                            answer: "Java"
                                                        },
                                                        {
                                                            answer: "Python"
                                                        },
                                                        {
                                                            answer: "C#"
                                                        },
                                                        {
                                                            answer: "Node"
                                                        }
                                                    ]
                                                }
                                                
                                            },
                                            {
                                                answer: "No"
                                            }
                                        ]
                                    }
                                    
                                }
                            ]
                        }
                    },
                    {
                        answer: "No",
                        next: {}
                    }
                ]
            },
            {
                question: "Test?",
                answers: [
                    {
                        answer: "Yes",
                        next: {}
                    },
                    {
                        answer: "No",
                        next: {}
                    }
                ]
            },
        ];



        return (
            <div className="chart-test">

                {chart.map((quest, ind) => (
                    <ChartQuestion question={quest} key={ind} />
                ))}
                
            

            </div>
        );
    }
}

const ChartQuestion = (props) => {

    

    const text = props.question.question;
    
    const answers = props.question.answers ? props.question.answers  : [];

    console.log("Question", props.question, text, answers);
    
    return (
        <div className="question">
            <p>{text}</p>

            <div className="answers">

                {props.question.answers.map((ans, i) => (
                    <div className="answer" key={i}>
                        <p>{ans.answer}</p>
                        {ans.subQuestion && (
                            <ChartQuestion question={ans.subQuestion} />
                        )}
                    </div>
                ))}
            </div>
        </div>
    );

}