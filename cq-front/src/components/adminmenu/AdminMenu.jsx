import React from 'react';
import { observer, inject } from 'mobx-react';

import './admin-menu.scss';

@inject("store", "viewMachine")
@observer
export default class AdminMenu extends React.Component {

    handleMenuClick = (action) => {
        console.log(action);
        this.props.viewMachine.action(action);

    }

    render() {

        const loggedIn = this.props.store.ux.isLoggedIn();
        const isAdmin = this.props.store.ux.user.isAdmin();
        const questionary = this.props.store.ux.questionary ? this.props.store.ux.questionary.name : false;

        const loading = this.props.store.ux.loading;

        if(!isAdmin || this.props.store.ux.viewState === "ADMIN") {
            return null;
        }


        return (
            <nav className="admin-menu">
                
                <AdminMenuItem text="New Questionary" value="adminNewQuestionary" onClick={this.handleMenuClick} />
                <AdminMenuItem text="Edit Questionary" value="adminEditQuestionary" onClick={this.handleMenuClick} />

                <AdminMenuItem text="New Question" value="adminNewQuestion" onClick={this.handleMenuClick} />
                <AdminMenuItem text="Edit Question" value="adminEditQuestion" onClick={this.handleMenuClick} />
              

            </nav>
        )
    }
}


@inject("viewMachine")
@observer
class AdminMenuItem extends React.Component {

    handleMenuClick = () => {
        this.props.onClick(this.props.value);
    }

    render() {

        if(!this.props.viewMachine.hasAction(this.props.value))  {
            console.warn("Unknown action", this.props.value);
            return null;
        }

        return (
            <div className="admin-menu-item" onClick={this.handleMenuClick}>
                {this.props.text}
            </div>
        );
    }

}