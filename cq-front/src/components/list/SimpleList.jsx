import React from 'react';
import { observer } from 'mobx-react';

import './simple-list.scss';


@observer
export default class SimpleList extends React.Component {

    handleClick = (e) => {

    }

    render() {
        return (
            <div className="simple-list">
                <ListHeader options={this.props.options} />

                {this.props.list.map((item, ind) => (
                    <ListRow key={ind} item={item} options={this.props.options} selected={this.props.selected ? this.props.selected.id === item.id : false}/>
                ) )}
            </div>
        );
    }

}

@observer
class ListHeader extends React.Component {

    render() {
        return (
            <div className="simple-list-row header">
                {this.props.options.columns.map((col, ind) => (
                    <div className={["simple-list-cell", "header", ...col.styleClass].join(" ")} key={ind}>
                        {col.headerText}
                    </div>
                ))}
            </div>
        )
    }
}

@observer
class ListRow extends React.Component {

    handleOnClick = () => {
        if(this.props.options.click && this.props.options.click.fullRow === true){
            this.props.options.click.onClickHandler(this.props.item);
        }
    }

    render() {
        const classes = [
            "simple-list-row",
            this.props.selected ? "selected" : "",
            this.props.options.click && this.props.options.click.fullRow ? "clickable" : ""
        ].join(" ");
        return (
            <div className={classes} onClick={this.handleOnClick}>
                {this.props.options.columns.map((col, ind) => (
                    <ListCell item={this.props.item} column={col} key={ind}/>
                ))}
            </div>
        )
    }
}

@observer
class ListCell extends React.Component {

    render() {
        const { item, column, ...rest } = this.props;
        
        

        let text = Array.isArray(column.key) 
            ? column.key.reduce((obj, key) => obj[key], item)
            : item[column.key];
        
            if(column.type && column.type === "count") {
                text = text.length;
            }

            if(column.func) {
                text = column.func(text);
            }

                
        return (
            <div className={["simple-list-cell", ...column.styleClass].join(" ")}>
                {text}
            </div>
        )
    }
}
