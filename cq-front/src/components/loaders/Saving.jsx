import React from 'react';

const Saving = (props) => {
    return (
        <div className="laoder saving">
            <h2>SAVING...</h2>
        </div>
    );
};

export default Saving;