import React from 'react';
import { observer, inject } from 'mobx-react';
import { observable, action, computed } from 'mobx';
import { PieChart, Legend } from 'react-easy-chart';

import statisticsStore from '../../models/StatisticsStore';


import './question-item.scss';

@inject("store", "viewMachine", "ux")
@observer
export default class QuestionItem extends React.Component {

    @observable chartVisible = true;
    
    constructor(props) {
        super(props);

        this.listener = null;
    }

    componentDidMount() {
        this.listener = setInterval(this.switchMode, 8000);
    }

    componentWillUnmount() {
        if(this.listener) {
            clearInterval(this.listener);
        }
        
    }

    @action
    switchMode = () => {
        this.chartVisible = !this.chartVisible;
    };

    render() {
        const question = this.props.question;
        const answersToQuestion = this.props.question.answers;

        const totalAnswersToThisQuestions = this.props.answers.filter(a => {
            if (typeof a.question === "string") {
                return a.question === question.id
            }
            return a.question.id === question.id
        }).length;

        const answersCounted = answersToQuestion.map(ans => {
            return {
                key: ans.text,
                value: this.props.answers.filter(a => {
                    if (typeof a.answer === "string") {
                        return a.answer === ans.id
                    }
                    return a.answer.id === ans.id
                }).length
            }
        });

        return (
            <div className="question-item">
                <h2>{question.question}</h2>


                {this.chartVisible && (
                    <div className="chart">
                    <PieChart size={120} data={answersCounted} />

                    <Legend data={answersCounted} dataId={'key'} horizontal />
                </div>
                )}
                
                {!this.chartVisible && (
                <div className="answer-items">
                    {answersToQuestion.map((answer) => (
                        <AnswerItem answer={answer} key={answer.id} answers={this.props.answers} total={totalAnswersToThisQuestions}/>
                    ))}

                    <div className="answer-item total">
                    <span className="answer-text">Total:</span>
                    <span className="answer-value">{totalAnswersToThisQuestions}</span>
                    </div>
                </div>
                )}




            </div>
        )
    }

}

@inject("store")
@observer
class AnswerItem extends React.Component {

    render() {

        const userAnswers = this.props.answers.filter(ans => {
            if (typeof ans.answer === "string") {
                return ans.answer === this.props.answer.id
            }
            return ans.answer.id === this.props.answer.id
        });
        // console.log("ANSWERS:", this.props.answers, userAnswers, this.props.answer);

        const perc = Math.round((userAnswers.length / this.props.total)*1000)/10;

        return (
            <div className="answer-item">
                <span className="answer-text">{this.props.answer.text}</span>
                <span className="answer-value">{userAnswers.length}</span>
                <span className="answer-perc">{perc}%</span>
            </div>
        )
    }
}

