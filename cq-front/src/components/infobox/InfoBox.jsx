import React from 'react';
import {observable, action } from 'mobx';
import {observer, inject} from 'mobx-react';

import CONFIG from '../../config';

import './info-box.scss';

@inject("store", "viewMachine", "ux")
@observer
export default class InfoBox extends React.Component {

    render() {

        if(!CONFIG.showInfoBox) {
            return null;
        }

        if(this.props.ux.isLoading()) {
            console.log("Still loading!");
        }

        return (
            <div className="info-box">
                
                USER: {this.props.store.ux.adminMode && (<b>ADMIN </b> )}<b>{this.props.store.ux.user.id}</b> / <b>{this.props.store.ux.user.role}</b><br />
                UX: <b>{this.props.ux.mode}</b> / <b>{this.props.ux.state}</b><br />
                STORE: <b>{this.props.store.ux.viewMode}</b> / <b>{this.props.store.ux.viewState}</b>
            </div>
        )
    }
}