import React from 'react';
import { observer } from 'mobx-react';

import notificationService from './notificationService';

import './notifier.scss';

@observer
export default class NotificationContainer extends React.Component {

    render() {
        return (
            <div className="notification-container">
                {notificationService.notifications.map(notification => (
                    <NotificationItem key={notification.id} notification={notification} />
                ))}
            </div>
        );
    }
}

const NotificationItem = props => {
    
    const classes= ["notification", props.notification.msgType].join(" ");

    return (
        <div className={classes}>
            {props.notification.message}
        </div>
    )
}