import { observable, action } from 'mobx';
import uuid from 'uuid';

import CONFIG from '../../config';

class NotificationModel {

    @observable notifications = [];

    @action
    _createNotificationObject(type, msg) {
        const msgObj = {
            id: uuid.v4(),
            msgType: type,
            message: msg
        }
        this.notifications.push(msgObj);
        setTimeout(() => {
            this._removeNotification(msgObj);
        }, CONFIG.notificationLiveTime);
    }

    @action
    _removeNotification(msgObj) {
        const ind = this.notifications.find(ntf => ntf.id === msgObj.id);
        this.notifications.remove(ind);
    }

    error(msg) {
        this._createNotificationObject("ERROR", msg);
    }

    debug(msg) {
        this._createNotificationObject("DEBUG", msg);
    }

    info(msg) {
        this._createNotificationObject("INFO", msg);
    }

    success(msg) {
        this._createNotificationObject("SUCCESS", msg);
    }

    warn(msg) {
        this._createNotificationObject("WARN", msg);
    }
}

const notificationService = new NotificationModel();
export default notificationService;