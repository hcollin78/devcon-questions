import React from 'react';
import { observer, inject } from 'mobx-react';

import './header.scss';

@inject("store", "viewMachine")
@observer
export default class Header extends React.Component {

    login = () => {
        const admin = this.props.store.getUserById("admin");
        console.log("LOGIN!", admin);
        if (admin) {
            this.props.store.ux.setUser(admin);
        }
    };

    logout = () => {
        this.props.store.ux.logout();
        
    };

    render() {
        
        let loggedIn = this.props.store.ux.isLoggedIn();

        return (
            <header className="main-header">
                <div className="user">
                    {loggedIn && (
                        <React.Fragment>
                        <h1>{this.props.store.ux.user.name}</h1>
                        <button onClick={this.logout}>Logout</button>
                        </React.Fragment>
                    )}
                    {!loggedIn && (
                        <button onClick={this.login}>Login</button>
                    )}
                </div>
                


                
            </header>
        )

    }
}